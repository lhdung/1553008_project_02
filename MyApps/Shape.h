#pragma once
#include"stdafx.h"
class Canvas;
class Shape
{
protected:
	bool isBeingSelected;
	COLORREF color;
	UINT type;
public:
	Shape()
	{
		isBeingSelected = false;
		color = RGB(0, 0, 0);
	}
	Shape(COLORREF color)
	{
		isBeingSelected = false;
		this->color = color;
	}
	virtual void draw(HDC hdc) = 0; //Draw an object using the parent window HDC
	virtual bool isClickToSelectNormal(int xClient, int yClient) = 0;
	virtual bool isClickToSelectEnhanced(int xClient, int yClient) = 0;
	virtual bool isClickToSelect(int xClient, int yClient) = 0;
	virtual void move(int oldXClient, int oldYClient, int xClient, int yClient) = 0;
	virtual void setBeingSelected(bool flag) = 0;
	virtual void setColor(COLORREF color) = 0;
	virtual void edit(HDC hdc,int oldXClient, int oldYClient, int xClient, int yClient) = 0;
	virtual bool isClickToEdit(int xClient, int yClient) = 0;
	virtual void drawSelectedStyle(HDC hdc) = 0;
	virtual void setData(int x1, int y1, int x2, int y2) = 0;
	virtual bool saveToFile(HANDLE hFile) = 0;
	virtual bool loadFromFile(HANDLE hFile) = 0;
	virtual Shape* cloneRetPtr() = 0;
	virtual UINT size() = 0;
	virtual void setAnchor(int x, int y) = 0;
	virtual UINT getType()
	{
		return type;
	}
	virtual COLORREF getColor() {
		return color;
	}
	friend class Canvas;
};
