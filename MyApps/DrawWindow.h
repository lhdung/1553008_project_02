#pragma once
#include "stdafx.h"
#include "resource.h"
#include "Shape.h"
#include "Canvas.h"
#include "AppsDef.h"
extern UINT iFormat;
struct DLGSTRUCT
{
	Canvas* canvas;
	WCHAR* buffer;
	UINT sizeBuffer;
};
ATOM RegisterDrawWindow(HINSTANCE hInst);
LRESULT CALLBACK DrawWindowProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
HWND CreateChildWindow(HWND hMDIClient, int numOfWindow, HANDLE hFile, WCHAR szFileName[MAX_TEXT]);
void CheckDrawmode(HWND hMDIClient, HMENU hMenu, UINT drawMode);
void CheckEditmode(HWND hMDIClient, HMENU hMenu, UINT editMode);
Canvas* GetWindowData(HWND hWnd);
void OnMouseClick(HWND hWnd, WPARAM wParam, LPARAM lParam);
void OnMouseMove(HWND hWNd, WPARAM wParam, LPARAM lParam);
void OnMouseRelease(HWND hWnd, WPARAM wParam, LPARAM lParam);
void OnMouseDoubleClick(HWND hWnd, WPARAM wParam, LPARAM lParam);
void OnSave(HWND hWnd, WPARAM wParam, LPARAM lParam);
void OnOpen(HWND hWnd, WPARAM wParam, LPARAM lParam, HWND hMDIClient, int numOfWindows);
bool OnCopy(HWND hWnd);
bool OnCut(HWND hWnd);
bool OnPaste(HWND hWnd);
bool OnDelete(HWND hWnd);
INT_PTR CALLBACK EditDlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);