#pragma once
#include "Shape.h"
#include "Resource.h"
#include "AppsDef.h"
class EllipseObj : public Shape
{
public:
	/*-------------------Constructor and Destructor------------*/
	EllipseObj(int color);
	EllipseObj();
	~EllipseObj();
	/*--------------------Set data method------------------------*/
	void setColor(COLORREF color);
	void setBeingSelected(bool flag);
	void setData(int x1, int y1, int x2, int y2);
	void setAnchor(int x, int y);
	Shape* cloneRetPtr();
	UINT size();
	/*--------------------Draw method----------------------------*/
	void draw(HDC hdc);
	void drawSelectedStyle(HDC hdc);

	/*--------------------Manipulating method---------------------*/
	void move(int oldXClient, int oldYClient, int xClient, int yClient);
	void edit(HDC hdc,int oldXClient, int oldYClient, int xClient, int yClient);
	bool isClickToSelect(int xClient, int yClient);
	bool isClickToSelectNormal(int xClient, int yClient);
	bool isClickToSelectEnhanced(int xClient, int yClient);

	bool isClickToEdit(int xClient, int yClient);

	/*--------------------File manipulating method----------------*/
	bool saveToFile(HANDLE hFile);
	bool loadFromFile(HANDLE hFile);
	//------------------------Class Member-------------------------
	RECT rect; //The rectangle contain the ellipse, can use this to construct ellipse region
	UINT _recentCorner;
	HPEN _pen;
};