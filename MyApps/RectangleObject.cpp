#include "stdafx.h"
#include "RectangleObject.h"
#include "Resource.h"
#include "UtilityFunctions.h"
#include "AppsDef.h"
RectObj::RectObj(int color) : Shape(color)
{
	this->type = RECTANGLE;
	this->_pen = NULL;
	this->_recentCorner = {};
}
RectObj::RectObj()
{
	this->type = RECTANGLE;
	this->color = RGB(0, 0, 0);
	this->_pen = NULL;
	this->_recentCorner = {};
}
RectObj::~RectObj()
{
	DeleteObject(this->_pen);
}
void RectObj::draw(HDC hdc)
{
	//Select it into HDC and keep the current Pen of the HDC
	HPEN curPen = NULL;
	HPEN selectSquarePen = NULL;
	//Draw rectangle
	//If it is being selected, we have to draw 4 squares in 4 corners
	if (isBeingSelected)
	{
		selectSquarePen = CreatePen(PS_SOLID, FOCUS_SQUARE_PEN_WIDTH, RGB(45, 114, 178));
		curPen = (HPEN)SelectObject(hdc, selectSquarePen);
		RectObj::drawSelectedStyle(hdc);
	}
	else
	{
		curPen = (HPEN)SelectObject(hdc, this->_pen);
		Rectangle(hdc, rect.left, rect.top, rect.right, rect.bottom);
	}
	//Attach the current Pen back to the HDC
	SelectObject(hdc, curPen);
	//Delete the Pen
	DeleteObject(selectSquarePen);
}
bool RectObj::isClickToSelect(int xClient, int yClient)
{
	if (isBeingSelected)
	{
		return RectObj::isClickToSelectNormal(xClient, yClient);
	}
	return RectObj::isClickToSelectEnhanced(xClient, yClient);
}

bool RectObj::isClickToSelectNormal(int xClient, int yClient)
{
	normalizeRect(&rect);

	POINT pt;
	pt.x = xClient;
	pt.y = yClient;
	RECT outsideRect;
	outsideRect.left = this->rect.left - EPSILON_CLICK_POINT_RECT;
	outsideRect.top = this->rect.top - EPSILON_CLICK_POINT_RECT;
	outsideRect.right = this->rect.right + EPSILON_CLICK_POINT_RECT;
	outsideRect.bottom = this->rect.bottom + EPSILON_CLICK_POINT_RECT;
	if (PtInRect(&outsideRect, pt))
		return true;
	return false;
}

bool RectObj::isClickToSelectEnhanced(int xClient, int yClient)
{
	normalizeRect(&rect);

	RECT outsideRect;
	RECT insideRect;
	//Initialize outside Rect
	outsideRect.left = this->rect.left - EPSILON_CLICK_POINT_RECT;
	outsideRect.top = this->rect.top - EPSILON_CLICK_POINT_RECT;
	outsideRect.right = this->rect.right + EPSILON_CLICK_POINT_RECT;
	outsideRect.bottom = this->rect.bottom + EPSILON_CLICK_POINT_RECT;
	//Initialize inside Rect
	insideRect.left = this->rect.left + EPSILON_CLICK_POINT_RECT;
	insideRect.top = this->rect.top + EPSILON_CLICK_POINT_RECT;
	insideRect.right = this->rect.right - EPSILON_CLICK_POINT_RECT;
	insideRect.bottom = this->rect.bottom - EPSILON_CLICK_POINT_RECT;
	POINT pt;
	pt.x = xClient;
	pt.y = yClient;
	if (PtInRect(&outsideRect, pt) && !PtInRect(&insideRect, pt))
	{
		return true;
	}
	else
	{
		return false;
	}
}

void RectObj::move(int oldXClient, int oldYClient, int xClient, int yClient)
{
	int deltaX = xClient - oldXClient;
	int deltaY = yClient - oldYClient;
	this->rect.left += deltaX;
	this->rect.top += deltaY;
	this->rect.right += deltaX;
	this->rect.bottom += deltaY;
}

void RectObj::drawSelectedStyle(HDC hdc)
{
	DrawFocusRect(hdc, &rect);
	RECT lftpRect;
	RECT rgtpRect;
	RECT lfbtRect;
	RECT rgbtRect;

	//Initialize left top corner rect
	POINT pt;
	pt.x = rect.left;
	pt.y = rect.top;
	fillRectAroundPoint(&lftpRect, pt, FOCUS_SQUARE_SIZE);
	//Initialize right top corner rect
	pt.x = rect.right;
	pt.y = rect.top;
	fillRectAroundPoint(&rgtpRect, pt, FOCUS_SQUARE_SIZE);
	//Initilize left bottom corner rect
	pt.x = rect.left;
	pt.y = rect.bottom;
	fillRectAroundPoint(&lfbtRect, pt, FOCUS_SQUARE_SIZE);
	//Initialize right bottom corner rect
	pt.x = rect.right;
	pt.y = rect.bottom;
	fillRectAroundPoint(&rgbtRect, pt, FOCUS_SQUARE_SIZE);
	//Draw the squares
	Rectangle(hdc, lftpRect.left, lftpRect.top, lftpRect.right, lftpRect.bottom);
	Rectangle(hdc, lfbtRect.left, lfbtRect.top, lfbtRect.right, lfbtRect.bottom);
	Rectangle(hdc, rgtpRect.left, rgtpRect.top, rgtpRect.right, rgtpRect.bottom);
	Rectangle(hdc, rgbtRect.left, rgbtRect.top, rgbtRect.right, rgbtRect.bottom);
}

void RectObj::setBeingSelected(bool flag)
{
	this->isBeingSelected = flag;
}

void RectObj::setColor(COLORREF color)
{
	if (this->color == color && this->_pen != NULL)
		return;
	this->color = color;
	DeleteObject(this->_pen);
	this->_pen = CreatePen(PS_SOLID, PEN_WIDTH, this->color);
}

bool RectObj::isClickToEdit(int xClient, int yClient)
{
	RECT smallRect;
	POINT ptClick;
	ptClick.x = xClient;
	ptClick.y = yClient;
	setBeingSelected(true);
	//Initialize left top corner rect
	POINT pt;
	pt.x = rect.left;
	pt.y = rect.top;
	fillRectAroundPoint(&smallRect, pt, FOCUS_SQUARE_SIZE);
	if (PtInRect(&smallRect, ptClick))
	{
		this->_recentCorner = CORNER_LT;
		return true;
	}
	//Initialize right top corner rect
	pt.x = rect.right;
	pt.y = rect.top;
	fillRectAroundPoint(&smallRect, pt, FOCUS_SQUARE_SIZE);
	if (PtInRect(&smallRect, ptClick))
	{
		this->_recentCorner = CORNER_RT;
		return true;
	}
	//Initilize left bottom corner rect
	pt.x = rect.left;
	pt.y = rect.bottom;
	fillRectAroundPoint(&smallRect, pt, FOCUS_SQUARE_SIZE);
	if (PtInRect(&smallRect, ptClick))
	{
		this->_recentCorner = CORNER_LB;
		return true;
	}
	//Initialize right bottom corner rect
	pt.x = rect.right;
	pt.y = rect.bottom;
	fillRectAroundPoint(&smallRect, pt, FOCUS_SQUARE_SIZE);
	if (PtInRect(&smallRect, ptClick))
	{
		this->_recentCorner = CORNER_RB;
		return true;
	}
	return false;
}

void RectObj::edit(HDC hdc,int oldXClient, int oldYClient, int xClient, int yClient)
{
	int deltaX = xClient - oldXClient;
	int deltaY = yClient - oldYClient;
	if (this->_recentCorner == CORNER_LT)
	{
		rect.left += deltaX;
		rect.top += deltaY;
		if (rect.left > rect.right)
		{
			if (rect.top < rect.bottom)
			{
				this->_recentCorner = CORNER_RT;
			}
			else
			{
				this->_recentCorner = CORNER_RB;
			}
		}
		else
		{
			if (rect.top > rect.bottom)
				this->_recentCorner = CORNER_LB;
		}
	}
	else if (this->_recentCorner == CORNER_RB)
	{
		rect.right += deltaX;
		rect.bottom += deltaY;
		if (rect.left > rect.right)
		{
			if (rect.top < rect.bottom)
			{
				this->_recentCorner = CORNER_LB;
			}
			else
			{
				this->_recentCorner = CORNER_LT;
			}
		}
		else
		{
			if (rect.top > rect.bottom)
				this->_recentCorner = CORNER_RT;
		}
	}
	else if (this->_recentCorner == CORNER_LB)
	{
		rect.left += deltaX;
		rect.bottom += deltaY;
		if (rect.left > rect.right)
		{
			if (rect.top < rect.bottom)
			{
				this->_recentCorner = CORNER_RB;
			}
			else
			{
				this->_recentCorner = CORNER_RT;
			}
		}
		else
		{
			if (rect.top > rect.bottom)
				this->_recentCorner = CORNER_LT;
		}
	}
	else if (this->_recentCorner == CORNER_RT)
	{
		rect.right += deltaX;
		rect.top += deltaY;
		if (rect.left > rect.right)
		{
			if (rect.top < rect.bottom)
			{
				this->_recentCorner = CORNER_LT;
			}
			else
			{
				this->_recentCorner = CORNER_LB;
			}
		}
		else
		{
			if (rect.top > rect.bottom)
				this->_recentCorner = CORNER_RB;
		}
	}
	normalizeRect(&this->rect);
}

void RectObj::setData(int x1, int y1, int x2, int y2)
{
	rect.left = x1;
	rect.top = y1;
	rect.right = x2;
	rect.bottom = y2;
}

void RectObj::setAnchor(int x, int y)
{
	auto width = rect.right - rect.left;
	auto height = rect.bottom - rect.top;
	rect.left = x;
	rect.top = y;
	rect.right = rect.left + width;
	rect.bottom = rect.top + height;
}

bool RectObj::saveToFile(HANDLE hFile)
{
	normalizeRect(&this->rect);
	if (!WriteFile(hFile, &this->type, sizeof(UINT), NULL, NULL))
		return false;
	if (!WriteFile(hFile, &this->color, sizeof(COLORREF), NULL, NULL))
		return false;
	if (!WriteFile(hFile, &this->rect, sizeof(RECT), NULL, NULL))
		return false;
	return true;
}

bool RectObj::loadFromFile(HANDLE hFile)
{
	COLORREF color;
	if (!ReadFile(hFile, &color, sizeof(COLORREF), NULL, NULL))
		return false;
	RectObj::setColor(color);
	if (!ReadFile(hFile, &this->rect, sizeof(RECT), NULL, NULL))
		return false;
	return true;
}

Shape * RectObj::cloneRetPtr()
{
	auto copObj = new RectObj();
	copObj->setColor(this->color);
	std::random_device rd;
	std::mt19937 mt(rd());
	std::uniform_real_distribution<double> gen(-3, 3);
	auto width = this->rect.right - this->rect.left;
	auto height = this->rect.bottom - this->rect.top;
	auto delta = gen(mt)*PADDING_CC;
	auto delta1 = gen(mt)*PADDING_CC;
	((RectObj*)copObj)->rect.left = rect.left + delta;
	((RectObj*)copObj)->rect.top = rect.top + delta1;
	((RectObj*)copObj)->rect.right = ((RectObj*)copObj)->rect.left + width;
	((RectObj*)copObj)->rect.bottom = ((RectObj*)copObj)->rect.top + height;
	return copObj;
}

UINT RectObj::size()
{
	return sizeof(RectObj);
}

