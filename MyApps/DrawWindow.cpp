#include "stdafx.h"
#include "DrawWindow.h"
#include "EllipseObject.h"
#include "RectangleObject.h"
#include "LineObject.h"
#include "TextObject.h"
#include "UtilityFunctions.h"
#include "AppsDef.h"
#include <CommCtrl.h>
HINSTANCE _hInst;
ATOM RegisterDrawWindow(HINSTANCE hInst);
LRESULT CALLBACK DrawWindowProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
ATOM RegisterDrawWindow(HINSTANCE hInst)
{
	_hInst = hInst;
	WNDCLASS wndClass;
	ZeroMemory(&wndClass, sizeof(wndClass));
	wndClass.style = CS_HREDRAW | CS_VREDRAW |  CS_DBLCLKS;
	wndClass.hInstance = hInst;
	wndClass.hCursor = LoadCursor(hInst, IDC_ARROW);
	wndClass.hIcon = LoadIcon(NULL, MAKEINTRESOURCE(IDI_MYAPPS));
	wndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wndClass.lpszMenuName = NULL;
	wndClass.lpszClassName = L"TEXT_WINDOW";
	wndClass.cbWndExtra = 4;
	wndClass.lpfnWndProc = (WNDPROC)DrawWindowProc;
	return RegisterClass(&wndClass);
}

LRESULT CALLBACK DrawWindowProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	static HMENU hMenu = NULL;
	switch (msg)
	{
	case WM_MDIACTIVATE:
	{
		OutputDebugStringA("WM_MDIACTIVATE\n");
		return 0;
	}
	case WM_SIZE:
	{
		OutputDebugStringA("WM_SIZE\n");
		auto canvas = GetWindowData(hWnd);
		if (wParam == SIZE_MAXIMIZED)
			canvas->_isMaximized = true;
		else
			canvas->_isMaximized = false;
		break;
	}
	case WM_CREATE:
	{
		auto canvas = (Canvas*)VirtualAlloc(NULL, sizeof(Canvas), MEM_COMMIT, PAGE_READWRITE);
		auto lpStruct = (LPMDICREATESTRUCT)((LPCREATESTRUCT)lParam)->lpCreateParams;
		if ((HANDLE)lpStruct->lParam == INVALID_HANDLE_VALUE)
		{
			//Initialize data for drawing window
			canvas->initResource();
		}
		else
		{
			canvas->setFileHandle((HANDLE)lpStruct->lParam);
			canvas->loadDataFromFile();
		}
		canvas->_hWnd = hWnd;
		SetWindowLong(hWnd, 0, (LONG)canvas);
		//Get the HMENU of mainMenu
		hMenu = GetMenu(GetParent(GetParent(hWnd)));
		CheckDrawmode(hWnd, hMenu, canvas->getDrawMode());
		CheckEditmode(hWnd, hMenu, canvas->getEditMode());
		return 0;
	}
	case WM_COMMAND:
	{
		switch (LOWORD(wParam))
		{
		case IDM_FILE_NEW:
		{
			PostMessageA(GetParent(GetParent(hWnd)), msg, wParam, lParam);
			break;
		}
		case IDM_FILE_OPEN:
			break;
		case IDM_FILE_SAVE:
			OnSave(hWnd, wParam, lParam);
			break;
		case IDM_DRAW_LINE:
		case IDM_DRAW_RECTANGLE:
		case IDM_DRAW_ELLIPSE:
		case IDM_DRAW_TEXT:
		{
			auto canvas = GetWindowData(hWnd);
			canvas->setDrawMode(LOWORD(wParam));
			CheckDrawmode(hWnd, hMenu, canvas->getDrawMode());
			break;
		}
		case ID_EDIT_DRAWMODE:
		case ID_EDIT_SELECTIONMODE:
		{
			auto canvas = GetWindowData(hWnd);
			canvas->setEditMode(LOWORD(wParam));
			CheckEditmode(hWnd, hMenu, canvas->getEditMode());
			break;
		}
		case ID_EDIT_COPY:
		{
			OnCopy(hWnd);
			break;
		}
		case ID_EDIT_CUT:
		{
			OnCut(hWnd);
			InvalidateRect(hWnd,NULL,TRUE);
			UpdateWindow(hWnd);
			break;
		}
		case ID_EDIT_PASTE:
		{
			OnPaste(hWnd);
			InvalidateRect(hWnd, NULL, TRUE);
			UpdateWindow(hWnd);

			break;
		}
		case ID_EDIT_DELETE:
		{
			OnDelete(hWnd);
			InvalidateRect(hWnd, NULL, TRUE);
			UpdateWindow(hWnd);
			break;
		}
		default:
			break;
		}
		return DefMDIChildProc(hWnd, msg, wParam, lParam);
	}
	case WM_KEYDOWN:
	{
		if (wParam == VK_ESCAPE)
		{
			auto canvas = GetWindowData(hWnd);
			canvas->deselSelectObject();
			InvalidateRect(hWnd, NULL, TRUE);
			UpdateWindow(hWnd);
		}
		break;
	}
	case WM_LBUTTONDBLCLK:
	{
		OnMouseDoubleClick(hWnd, wParam, lParam);
		break;
	}
	case WM_LBUTTONUP:
	{
		OutputDebugStringA("Mouse Released Recevied\n");
		OnMouseRelease(hWnd, wParam, lParam);
		break;
	}
	case WM_LBUTTONDOWN:
	{
		OutputDebugStringA("Mouse Click Received\n");
		OnMouseClick(hWnd, wParam, lParam);
		break;
	}
	case WM_MOUSEMOVE:
	{
		OnMouseMove(hWnd, wParam, lParam);
		break;
	}
	case WM_CLOSE:
	{
		auto canvas = GetWindowData(hWnd);
		if (!canvas->_isSaved)
		{
			if (MessageBox(hWnd, L"You data are not saved. Do you really want to quit it?", L"Warning", MB_YESNO | MB_ICONWARNING) == IDNO)
			{
				return 0;
			}
		}
		canvas->freeResource();
		break;
	}
	case WM_PAINT:
	{
		OutputDebugStringA("WM_PAINT\n");
		auto canvas = GetWindowData(hWnd);
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);
		//Set to hollow brush
		SelectObject(hdc, GetStockObject(HOLLOW_BRUSH));
		canvas->draw(hdc);
		EndPaint(hWnd, &ps);
		break;
	}
	default:
		break;
	}
	return DefMDIChildProc(hWnd, msg, wParam, lParam);
}

HWND CreateChildWindow(HWND hMDIClient, int numOfWindows, HANDLE hFile,WCHAR szFileName[MAX_TEXT])
{
	WCHAR szName[MAX_TEXT];
	if (hFile != INVALID_HANDLE_VALUE)
		lstrcpyW(szName, szFileName);
	else
		wsprintf(szName, L"Noname-%d.drw", numOfWindows);
	MDICREATESTRUCT mdiCrStr;
	mdiCrStr.hOwner = _hInst;
	mdiCrStr.szClass = L"TEXT_WINDOW";
	mdiCrStr.cx = CW_USEDEFAULT;
	mdiCrStr.cy = CW_USEDEFAULT;
	mdiCrStr.x = CW_USEDEFAULT;
	mdiCrStr.y = CW_USEDEFAULT;
	mdiCrStr.szTitle = szName;
	mdiCrStr.style = 0;
	mdiCrStr.lParam = (LPARAM)hFile;
	return (HWND)SendMessage(hMDIClient, WM_MDICREATE, 0, (LPARAM)&mdiCrStr);
}

void CheckDrawmode(HWND hMDIClient, HMENU hMenu, UINT drawMode)
{
	auto canvas = GetWindowData(hMDIClient);
	CheckMenuRadioItem(GetSubMenu(hMenu, canvas->_isMaximized ? DRAWMENU_POS + 1 : DRAWMENU_POS), DRAWMODE_LINE, DRAWMODE_TEXT, drawMode, MF_BYCOMMAND);
	HWND hToolbar =(HWND) SendMessage(GetParent(GetParent(hMDIClient)), WM_GETTOOLBAR, 0, 0);
	SendMessage(hToolbar, TB_CHECKBUTTON, drawMode, TRUE);
}

void CheckEditmode(HWND hMDIClient, HMENU hMenu, UINT editMode)
{
	auto canvas = GetWindowData(hMDIClient);
	CheckMenuRadioItem(GetSubMenu(hMenu, canvas->_isMaximized ? EDITMENU_POS + 1 : EDITMENU_POS), EDITMODE_DRAW, EDITMODE_SELECT, editMode, MF_BYCOMMAND);
	HWND hToolbar = (HWND)SendMessage(GetParent(GetParent(hMDIClient)), WM_GETTOOLBAR, 0, 0);
	SendMessage(hToolbar, TB_CHECKBUTTON, editMode, TRUE);
}

Canvas * GetWindowData(HWND hWnd)
{
	return (Canvas*)GetWindowLong(hWnd, 0);
}

void OnMouseClick(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	//When user click, we not create the object immediately, instead we keep track of that click
	//We will create the object in OnMouseMove, that's where people will drag the mouse to draw new object
	auto canvas = GetWindowData(hWnd);
	int xClient = LOWORD(lParam);
	int yClient = HIWORD(lParam);
	canvas->_xRecentClick = xClient;
	canvas->_yRecentClick = yClient;
	if (canvas->getEditMode() == EDITMODE_DRAW)
	{
		if (canvas->getDrawMode() == DRAWMODE_TEXT)
		{
			WCHAR buffer[MAX_TEXT] = {};
			DLGSTRUCT dlgStr;
			dlgStr.canvas = canvas;
			dlgStr.buffer = buffer;
			dlgStr.sizeBuffer = MAX_TEXT;
			if (DialogBoxParam(_hInst, MAKEINTRESOURCE(IDD_INPUTTEXT), hWnd, EditDlgProc, (LPARAM)&dlgStr)== IDOK)
			{
				if (buffer[0] != '\0')
				{
					TextObj* shape = new TextObj();
					shape->setColor(canvas->getDrawColor());
					shape->setFont(canvas->getFont());
					shape->setText(std::wstring(buffer));
					shape->setData(xClient, yClient, 0, 0);
					canvas->addNewObject(shape);
				}
			}
			InvalidateRect(hWnd, NULL, TRUE);
			return;
		}
		canvas->_isClickToDrawObject = true;
		canvas->_isClickToEditSelectObject = false;
		canvas->_isClickToMoveObject = false;
		if(canvas->deselSelectObject())
			InvalidateRect(hWnd, NULL, TRUE);
	}
	else if (canvas->getEditMode() == EDITMODE_SELECT)
	{
		auto selectedObject = canvas->getSelectedObject();
		if (selectedObject != nullptr && selectedObject->isClickToEdit(xClient, yClient))
		{
			canvas->_isClickToEditSelectObject = true;
			return;
		}
		auto result = canvas->setSelectObject(xClient, yClient);
		if(result != SAME_OBJECT)
			InvalidateRect(hWnd, NULL, TRUE);
		selectedObject = canvas->getSelectedObject();
		if (selectedObject == nullptr)
			return;
		if (selectedObject->isClickToEdit(xClient, yClient))
		{
			canvas->_isClickToEditSelectObject = true;
		}
		else
		{
			canvas->_isClickToMoveObject = true;
		}
		canvas->_isClickToDrawObject = false;
	}
	UpdateWindow(hWnd);
}

void OnMouseMove(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	auto canvas = GetWindowData(hWnd);
	int xClient = LOWORD(lParam);
	int yClient = HIWORD(lParam);
	//Consider if the new point is too near to the previous point
	POINT pt;
	pt.x = canvas->_xRecentClick;
	pt.y = canvas->_yRecentClick;
	RECT estimateRect;
	fillRectAroundPoint(&estimateRect, pt, ESTIMATED_POINT);
	POINT pt2;
	pt2.x = xClient;
	pt2.y = yClient;
	if (PtInRect(&estimateRect, pt2))
		return;
	//If the user hold the click and move
	if (wParam == MK_LBUTTON)
	{
		//If the previous mouse click is for moving selected object
		if (canvas->_isClickToMoveObject)
		{
			//Move the object
			canvas->moveSelectObject(xClient, yClient);
			canvas->_xRecentClick = xClient;
			canvas->_yRecentClick = yClient;
		}
		//If the previous mouse click is for editing selected object
		else if (canvas->_isClickToEditSelectObject)
		{
			canvas->editSelectedObject(xClient, yClient);
			canvas->_xRecentClick = xClient;
			canvas->_yRecentClick = yClient;
		}
		//If user try to draw a new object
		//This will processing drawing only for Rectangle, Ellipse , Line
		//With Text, we will create it with the click
		else if (canvas->_isClickToDrawObject)
		{
			//Check if it is the first time drawing
			//Create memory for it and let the canvas manage the object
			if (!canvas->_isObjectCreated)
			{
				OutputDebugStringA("Begin to Draw Object\n");
				canvas->_isObjectCreated = true;
				if (canvas->getDrawMode() == DRAWMODE_RECT)
				{
					RectObj* shape = new RectObj();
					shape->setData(canvas->_xRecentClick, canvas->_yRecentClick, xClient, yClient);
					shape->setColor(canvas->getDrawColor());
					canvas->addNewObject(shape);
				}
				else if (canvas->getDrawMode() == DRAWMODE_ELLIPSE)
				{
					EllipseObj* shape = new EllipseObj(canvas->getDrawColor());
					shape->setData(canvas->_xRecentClick, canvas->_yRecentClick, xClient, yClient);
					shape->setColor(canvas->getDrawColor());
					canvas->addNewObject(shape);
				}
				else if (canvas->getDrawMode() == DRAWMODE_LINE)
				{
					LineObj* shape = new LineObj(canvas->getDrawColor());
					shape->setData(canvas->_xRecentClick, canvas->_yRecentClick, xClient, yClient);
					shape->setColor(canvas->getDrawColor());
					canvas->addNewObject(shape);
				}
			}
			//If the object is already initilized, we just have to set its data instead of recreating it
			else
			{
				OutputDebugStringA("Drawing Object\n");
				if (canvas->getDrawMode() == DRAWMODE_RECT)
				{
					Shape* shape = canvas->getLatestObject();
					shape->setData(canvas->_xRecentClick, canvas->_yRecentClick, xClient, yClient);
				}
				else if (canvas->getDrawMode() == DRAWMODE_ELLIPSE)
				{
					Shape* shape = canvas->getLatestObject();
					shape->setData(canvas->_xRecentClick, canvas->_yRecentClick, xClient, yClient);
				}
				else if (canvas->getDrawMode() == DRAWMODE_LINE)
				{
					Shape* shape = canvas->getLatestObject();
					shape->setData(canvas->_xRecentClick, canvas->_yRecentClick, xClient, yClient);
				}
			}
		}
		if (canvas->_isClickToDrawObject || canvas->_isClickToEditSelectObject || canvas->_isClickToMoveObject)
		{
			canvas->_isSaved = false;
			InvalidateRect(hWnd, NULL, TRUE);
			UpdateWindow(hWnd);
		}
	}
}

void OnMouseRelease(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	auto canvas = GetWindowData(hWnd);
	int xClient = LOWORD(lParam);
	int yClient = HIWORD(lParam);

	canvas->_isClickToDrawObject = false;
	canvas->_isClickToMoveObject = false;
	canvas->_isClickToEditSelectObject = false;
	canvas->_isObjectCreated = false;
}

void OnMouseDoubleClick(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	auto canvas = GetWindowData(hWnd);
	int xClient = LOWORD(lParam);
	int yClient = HIWORD(lParam);
	auto selectedObj = canvas->getSelectedObject();
	if (selectedObj == nullptr)
		return;
	if (selectedObj->getType() == TEXT)
	{
		canvas->_isClickToEditSelectObject = true;
		WCHAR buffer[500] = {};
		StringCchCopy(buffer,500,reinterpret_cast<TextObj*>(canvas->getSelectedObject())->getText().c_str());
		DLGSTRUCT dlgStr;
		dlgStr.canvas = canvas;
		dlgStr.buffer = buffer;
		dlgStr.sizeBuffer = MAX_TEXT;
		if (DialogBoxParam(_hInst, MAKEINTRESOURCE(IDD_INPUTTEXT), hWnd, EditDlgProc, (LPARAM)&dlgStr))
		{
			if (buffer[0] == '\0')
			{
				//remove the object
			}
			else
			{
				reinterpret_cast<TextObj*>(canvas->getSelectedObject())->setText(std::wstring(buffer));
			}
		}
		
	}
	InvalidateRect(hWnd, NULL, TRUE);
	UpdateWindow(hWnd);
}

void OnSave(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	auto canvas = GetWindowData(hWnd);
	if (canvas->getFileHandle() != INVALID_HANDLE_VALUE)
	{
		SetFilePointer(canvas->getFileHandle(), 0, NULL, FILE_BEGIN);
		SetEndOfFile(canvas->getFileHandle());
		if (canvas->saveToFile())
			canvas->_isSaved = true;
		else
			canvas->_isSaved = false;
		return;
	}
	TCHAR szFilter[] = L"Draw file\0*.drw\0";
	TCHAR szFile[500] = L"Noname-untitled.drw";

	OPENFILENAME ofn{};
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = hWnd;
	ofn.lpstrFilter = szFilter;
	ofn.nFilterIndex = 1;
	ofn.lpstrFile = szFile;
	ofn.nMaxFile = sizeof(szFile) / sizeof(TCHAR);
	ofn.lpstrDefExt = L"drw";
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_EXPLORER;
	if (GetSaveFileName(&ofn))
	{
		OutputDebugStringA("Success to get file from Save as Dialog\n");
		auto file = CreateFile(ofn.lpstrFile, GENERIC_WRITE, NULL, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
		if (file == INVALID_HANDLE_VALUE)
		{
			MessageBoxA(hWnd, "Cannot save file now, maybe it is used by another application, try it again", "Error", MB_OK | MB_ICONERROR);
			return;
		}
		auto canvas = GetWindowData(hWnd);
		canvas->setFileHandle(file);
		if (canvas->saveToFile())
		{
			canvas->_isSaved = true;
			SetWindowText(hWnd, ofn.lpstrFile);
		}
		else
			canvas->_isSaved = false;
	}
	else
	{
		OutputDebugStringA("Cancel to save file\n");
	}
}

void OnOpen(HWND hWnd, WPARAM wParam, LPARAM lParam, HWND hMDIClient, int numOfWindows)
{
	TCHAR szFilter[] = L"Draw file\0*.drw\0";
	TCHAR szFile[500] = {};
	OPENFILENAME ofn{};
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = hWnd;
	ofn.lpstrFilter = szFilter;
	ofn.nFilterIndex = 1;
	ofn.lpstrFile = szFile;
	ofn.nMaxFile = sizeof(szFile) / sizeof(TCHAR);
	ofn.Flags = OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST;
	if (GetOpenFileName(&ofn))
	{
		auto file = CreateFile(ofn.lpstrFile, GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
		if (file == INVALID_HANDLE_VALUE)
		{
			MessageBoxA(hWnd, "Can't open file now, try it again\n", "Error", MB_OK);
			return;
		}
		CreateChildWindow(hMDIClient, numOfWindows, file,ofn.lpstrFile);
	}
}

bool OnCopy(HWND hWnd)
{
	auto canvas = GetWindowData(hWnd);
	if (canvas->getSelectedObject() == nullptr)
	{
		MessageBoxA(hWnd, "Nothing selected to be copied", "Error", MB_OK);
		return false;
	}
	auto selectObject = canvas->getSelectedObject();
	OpenClipboard(hWnd);
	EmptyClipboard();
	HANDLE hMem;
	hMem = GlobalAlloc(GMEM_MOVEABLE, selectObject->size());
	auto copObj =(RectObj*) GlobalLock(hMem);
	memcpy(copObj, selectObject, selectObject->size());
	GlobalUnlock(hMem);
	SetClipboardData(iFormat, hMem);
	CloseClipboard();
	canvas->_isCopy = true;
	return true;
}

bool OnCut(HWND hWnd)
{
	if (OnCopy(hWnd))
	{
		auto canvas = GetWindowData(hWnd);
		auto selectObject = canvas->getSelectedObject();
		canvas->remove(selectObject);
		canvas->_isCopy = false;
		return true;
	}
	return false;
}

bool OnPaste(HWND hWnd)
{
	auto canvas = GetWindowData(hWnd);

	OpenClipboard(hWnd);
	if (IsClipboardFormatAvailable(iFormat))
	{
		auto hMem = GetClipboardData(iFormat);
		auto obj = (Shape*)GlobalLock(hMem);
		Shape* newObj = obj->cloneRetPtr();
		if (!canvas->_isCopy)
		{
			newObj->setAnchor(canvas->_xRecentClick, canvas->_yRecentClick);
		}
		canvas->addNewObject(newObj);
		GlobalUnlock(hMem);
		
	}
	else if (IsClipboardFormatAvailable(CF_BITMAP))
	{
		auto hMem = GetClipboardData(CF_BITMAP);
		auto hBitmap = (HBITMAP)GlobalLock(hMem);
		auto hdc = GetDC(hWnd);
		auto memDC = CreateCompatibleDC(hdc);
		SelectObject(memDC, hBitmap);
		RECT rect;
		GetClientRect(hWnd, &rect);
		BitBlt(hdc, 0, 0, rect.right, rect.bottom, memDC, 0, 0, SRCCOPY);
		ReleaseDC(hWnd, hdc);
		DeleteDC(memDC);
		GlobalUnlock(hMem);
	}
	else if (IsClipboardFormatAvailable(CF_TEXT))
	{
		TextObj* newObj = new TextObj();
		auto hMem = GetClipboardData(CF_TEXT);
		auto str = (char*)GlobalLock(hMem);
		WCHAR wStr[MAX_TEXT];
		mbstowcs(wStr, str, strlen(str) + 1);
		std::wstring strData(wStr);
		newObj->setText(std::move(strData));
		newObj->setColor(canvas->getDrawColor());
		newObj->setFont(canvas->getFont());
		newObj->setData(canvas->_xRecentClick, canvas->_yRecentClick, 0, 0);
		canvas->addNewObject(newObj);
		GlobalUnlock(hMem);
	}
	CloseClipboard();
	return false;
}

bool OnDelete(HWND hWnd)
{
	auto canvas = GetWindowData(hWnd);
	auto selectObject = canvas->getSelectedObject();
	if (selectObject != nullptr)
	{
		canvas->remove(selectObject);
	}
	return true;
}

INT_PTR CALLBACK EditDlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	static Canvas* canvas = nullptr;
	static HWND editCtrl = NULL;
	static HFONT font = NULL;
	static WCHAR* buffer = nullptr;
	static UINT sizeBuffer = 0;
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
	{
		canvas = reinterpret_cast<DLGSTRUCT*>(lParam)->canvas;
		buffer = reinterpret_cast<DLGSTRUCT*>(lParam)->buffer;
		sizeBuffer = reinterpret_cast<DLGSTRUCT*>(lParam)->sizeBuffer;
		editCtrl = GetDlgItem(hDlg, IDC_TEXT);
		if (!canvas->_isClickToEditSelectObject)
			font = CreateFontIndirect(&canvas->getFont());
		else
			font = CreateFontIndirect(&reinterpret_cast<TextObj*>(canvas->getSelectedObject())->getFont());
		SendMessage(editCtrl, EM_SETLIMITTEXT, MAX_TEXT, 0);
		SendMessage(editCtrl, WM_SETFONT, (WPARAM)font, FALSE);
		SendMessage(editCtrl, WM_SETTEXT, 0, (LPARAM)buffer);
		return (INT_PTR)TRUE;
	}
	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			SendMessage(editCtrl, WM_GETTEXT, sizeBuffer, (LPARAM)buffer);
			DeleteObject(font);
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	case WM_CTLCOLOREDIT:
	{
		if (canvas->getSelectedObject() == nullptr)
			return (INT_PTR)FALSE;
		HDC hdc =(HDC) wParam;
		SetTextColor(hdc, canvas->getSelectedObject()->getColor());
		return (INT_PTR)GetStockObject(WHITE_BRUSH);
	}
	}
	
	return (INT_PTR)FALSE;
}