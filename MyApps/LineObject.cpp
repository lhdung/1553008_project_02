#include "stdafx.h"
#include "LineObject.h"
#include "Resource.h"
#include "UtilityFunctions.h"
#include "AppsDef.h"
LineObj::LineObj(COLORREF color) : Shape(color)
{
	this->type = LINE;
	this->_ptA = {};
	this->_ptB = {};
	this->_recentCorner = {};
	this->_pen = NULL;
}
LineObj::LineObj()
{
	this->color = RGB(0, 0, 0);
	this->type = LINE;
	this->_ptA = {};
	this->_ptB = {};
	this->_recentCorner = {};
	this->_pen = NULL;
}
LineObj::~LineObj()
{
	DeleteObject(this->_pen);
}
void LineObj::draw(HDC hdc)
{
	HPEN curPen = NULL;
	HPEN objSelectPen = NULL;
	curPen = (HPEN)SelectObject(hdc, this->_pen);
	MoveToEx(hdc, this->_ptA.x, this->_ptA.y, NULL);
	LineTo(hdc, this->_ptB.x, this->_ptB.y);
	if (isBeingSelected)
	{
		objSelectPen = CreatePen(PS_SOLID, 1, RGB(45, 114, 178));
		SelectObject(hdc, objSelectPen);
		drawSelectedStyle(hdc);
	}
	SelectObject(hdc, curPen);
	DeleteObject(objSelectPen);
}

bool LineObj::isClickToSelect(int xClient, int yClient)
{
	return LineObj::isClickToSelectNormal(xClient, yClient);
}

bool LineObj::isClickToSelectNormal(int xClient, int yClient)
{
	POINT pt;
	pt.x = xClient;
	pt.y = yClient;
	LineEquation line = constructLineEquation(this->_ptA, this->_ptB);
	if (distanceToLine(pt, line) <= EPSILON_CLICK_POINT_LINE)
	{
		POINT pt2 = findProjectionPoint(pt, line);
		if (length(pt2, this->_ptA) + length(pt2, this->_ptB) > length(this->_ptA, this->_ptB) + EPSILON_LENGTH)
		{
			return false;
		}
		return true;
	}
	return false;
}

bool LineObj::isClickToSelectEnhanced(int xClient, int yClient)
{
	return LineObj::isClickToSelectNormal(xClient, yClient);
}

void LineObj::move(int oldXClient, int oldYClient, int xClient, int yClient)
{
	int deltaX = xClient - oldXClient;
	int deltaY = yClient - oldYClient;
	this->_ptA.x += deltaX;
	this->_ptA.y += deltaY;
	this->_ptB.x += deltaX;
	this->_ptB.y += deltaY;
}

void LineObj::setBeingSelected(bool flag)
{
	this->isBeingSelected = flag;
}

void LineObj::setColor(COLORREF color)
{
	if (this->color == color && this->_pen != NULL)
		return;
	this->color = color;
	DeleteObject(this->_pen);
	this->_pen = CreatePen(PS_SOLID, PEN_WIDTH, this->color);
}

void LineObj::edit(HDC hdc,int oldXClient, int oldYClient, int xClient, int yClient)
{
	int deltaX = xClient - oldXClient;
	int deltaY = yClient - oldYClient;
	if (this->_recentCorner == CORNER_A)
	{
		this->_ptA.x += deltaX;
		this->_ptA.y += deltaY;
	}
	else if (this->_recentCorner == CORNER_B)
	{
		this->_ptB.x += deltaX;
		this->_ptB.y += deltaY;
	}
}

bool LineObj::isClickToEdit(int xClient, int yClient)
{
	POINT pt;
	pt.x = xClient;
	pt.y = yClient;
	RECT rect1;
	RECT rect2;
	fillRectAroundPoint(&rect1, this->_ptA, FOCUS_SQUARE_SIZE);
	fillRectAroundPoint(&rect2, this->_ptB, FOCUS_SQUARE_SIZE);
	if (PtInRect(&rect1, pt))
	{
		this->_recentCorner = CORNER_A;
		return true;
	}
	if (PtInRect(&rect2, pt))
	{
		this->_recentCorner = CORNER_B;
		return true;
	}
	return false;
}

void LineObj::drawSelectedStyle(HDC hdc)
{
	RECT rect1;
	RECT rect2;
	fillRectAroundPoint(&rect1, this->_ptA, FOCUS_SQUARE_SIZE);
	fillRectAroundPoint(&rect2, this->_ptB, FOCUS_SQUARE_SIZE);
	Rectangle(hdc, rect1.left, rect1.top, rect1.right, rect1.bottom);
	Rectangle(hdc, rect2.left, rect2.top, rect2.right, rect2.bottom);
}

void LineObj::setData(int x1, int y1, int x2, int y2)
{
	this->_ptA.x = x1;
	this->_ptA.y = y1;
	this->_ptB.x = x2;
	this->_ptB.y = y2;
}

void LineObj::setAnchor(int x, int y)
{
	auto deltaX = x - _ptA.x;
	auto deltaY = y - _ptA.y;
	_ptA.x = x;
	_ptA.y = y;
	_ptB.x += deltaX;
	_ptB.y += deltaY;
}

bool LineObj::saveToFile(HANDLE hFile)
{
	if (!WriteFile(hFile, &this->type, sizeof(UINT), NULL, NULL))
		return false;
	if (!WriteFile(hFile, &this->color, sizeof(COLORREF), NULL, NULL))
		return false;
	if (!WriteFile(hFile, &this->_ptA, sizeof(POINT), NULL, NULL))
		return false;
	if (!WriteFile(hFile, &this->_ptB, sizeof(POINT), NULL, NULL))
		return false;
	return true;
}

bool LineObj::loadFromFile(HANDLE hFile)
{
	COLORREF color;
	if (!ReadFile(hFile, &color, sizeof(COLORREF), NULL, NULL))
		return false;
	LineObj::setColor(color);
	if (!ReadFile(hFile, &this->_ptA, sizeof(POINT), NULL, NULL))
		return false;
	if (!ReadFile(hFile, &this->_ptB, sizeof(POINT), NULL, NULL))
		return false;
	return true;
}

Shape * LineObj::cloneRetPtr()
{
	auto copObj = new LineObj();
	copObj->setColor(this->color);
	std::random_device rd;
	std::mt19937 mt(rd());
	std::uniform_real_distribution<double> dis(-3, 3);
	auto delta = dis(mt)*PADDING_CC;
	auto delta1 = dis(mt)*PADDING_CC;
	((LineObj*)copObj)->_ptA.x = this->_ptA.x + delta;
	((LineObj*)copObj)->_ptA.y = this->_ptA.y + delta1;
	((LineObj*)copObj)->_ptB.x = this->_ptB.x + delta;
	((LineObj*)copObj)->_ptB.y = this->_ptB.y + delta1;
	return copObj;
}


UINT LineObj::size()
{
	return sizeof(LineObj);
}
