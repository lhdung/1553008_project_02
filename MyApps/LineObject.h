#pragma once
#include "Shape.h"
#include "Resource.h"
#include "AppsDef.h"
class LineObj : public Shape
{
public:
	LineObj(COLORREF color);
	LineObj();
	~LineObj();
	void draw(HDC hdc); //Draw an object using the parent window HDC
	bool isClickToSelect(int xClient, int yClient); //check the point that user click selecting any object
	bool isClickToSelectNormal(int xClient, int yClient);
	bool isClickToSelectEnhanced(int xClient, int yClient);
	void move(int oldXClient, int oldYClient, int xClient, int yClient);
	void setBeingSelected(bool flag);
	void setColor(COLORREF color);
	void edit(HDC hdc,int oldXClient, int oldYClient, int xClient, int yClient);
	bool isClickToEdit(int xClient, int yClient);
	void drawSelectedStyle(HDC hdc);
	void setData(int x1, int y1, int x2, int y2);
	void setAnchor(int x, int y);
	bool saveToFile(HANDLE hFile);
	bool loadFromFile(HANDLE hFile);
	Shape* cloneRetPtr();
	UINT size();
private:
	POINT _ptA;
	POINT _ptB;
	UINT _recentCorner;
	HPEN _pen;
};