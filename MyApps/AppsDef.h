#pragma once
#include "stdafx.h"
//Constant for Drawing Object
#define ESTIMATED_POINT 4 //To check if one point is too close to previous point
#define FOCUS_SQUARE_SIZE 4 //the size of edit box around the object
#define EPSILON_LENGTH 1 //The error to compares the length of lines
#define EPSILON_CLICK_POINT_LINE 5 //The max distance of one point to the line if user intend to select the line
#define EPSILON_CLICK_POINT_RECT 5
#define EPSILON_CLICK_POINT_ELLIPSE 5
#define PEN_WIDTH 2 //the width of the pen
#define FOCUS_SQUARE_PEN_WIDTH 1 //the width of the pen use to draw edit box
#define LEFT_FLAG 0 
#define RIGHT_FLAG 1
#define TOP_FLAG 0
#define BOT_FLAG 1
/*----------Draw mode constant------------*/
#define DRAWMODE_LINE IDM_DRAW_LINE
#define DRAWMODE_RECT IDM_DRAW_RECTANGLE
#define DRAWMODE_ELLIPSE IDM_DRAW_ELLIPSE
#define DRAWMODE_TEXT IDM_DRAW_TEXT
/*----------Edit Mode constant------------*/
#define EDITMODE_DRAW ID_EDIT_DRAWMODE
#define EDITMODE_SELECT ID_EDIT_SELECTIONMODE
/*---------The position of edit box for Rectangle, Ellipse-----------*/
#define CORNER_LT 0
#define CORNER_LB 1
#define CORNER_RT 2
#define CORNER_RB 3
/*--------The position of edit box for Line----------*/
#define CORNER_A 0
#define CORNER_B 1
/*------------------Type of Objects-------------------*/
#define RECTANGLE 1
#define ELLIPSE 2
#define TEXT 3
#define LINE 4
/*-----------------The menu position-----------------*/
#define DRAWMENU_POS 2
#define EDITMENU_POS 1
/*-----------------Text configuration for Input Text Dlg-----------*/
#define MAX_TEXT 500
/*-----------------Size of Focus Rectangle around the text-----------*/
#define TEXT_RECTANGLE_ADD 4
/*-----------------Use to know whether the new selected obj is the same or not---------------*/
#define SAME_OBJECT 1
#define DIFF_OBJECT 2
#define NONE 0
/*-----------------Application defined-message---------------*/
#define WM_GETTOOLBAR WM_APP
#define WM_CHILDCOPY WM_APP + 1
#define WM_CHILDCUT WM_APP + 2
#define WM_CHILDPASTE WM_APP + 3
#define WM_CHILDDELETE WM_APP + 4
/*-----------------For Edit Menu---------------------------*/
#define PADDING_CC 20
