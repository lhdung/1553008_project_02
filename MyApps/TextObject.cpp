#include "stdafx.h"
#include "TextObject.h"
#include "Resource.h"
#include "UtilityFunctions.h"
TextObj::TextObj(COLORREF color) : Shape(color)
{
	this->type = TEXT;
	this->_content = {};
	this->_font = {};
	this->rect = {};
	this->_hFont = NULL;
}
TextObj::TextObj()
{
	this->type = TEXT;
	this->_content = {};
	this->_font = {};
	this->rect = {};
	this->_hFont = NULL;
}
TextObj::~TextObj()
{
	DeleteObject(this->_hFont);
}
void TextObj::draw(HDC hdc)
{
	if (this->_content.empty())
	{
		return;
	}
	SetBkMode(hdc, TRANSPARENT);
	HFONT curFont = NULL;
	curFont = (HFONT)SelectObject(hdc, this->_hFont);
	//If this text is being selected, draw the focus rectangle around the text
	DrawText(hdc, this->_content.c_str(), this->_content.size(), &rect, DT_CALCRECT);
	if (this->isBeingSelected)
	{
		HPEN tmpPen = CreatePen(PS_SOLID, FOCUS_SQUARE_PEN_WIDTH, RGB(45, 114, 178));
		HPEN curPen = (HPEN)SelectObject(hdc, tmpPen);
		drawSelectedStyle(hdc);
		SelectObject(hdc, curPen);
		DeleteObject(tmpPen);
	}
	COLORREF oldColor = SetTextColor(hdc, color);
	DrawText(hdc, this->_content.c_str(), -1, &rect, DT_LEFT);
	SelectObject(hdc, curFont);
	SetTextColor(hdc, oldColor);
}

bool TextObj::isClickToSelect(int xClient, int yClient)
{
	return TextObj::isClickToSelectNormal(xClient, yClient);
}

bool TextObj::isClickToSelectNormal(int xClient, int yClient)
{
	POINT pt;
	pt.x = xClient;
	pt.y = yClient;
	if (isBeingSelected)
	{
		RECT rect;
		CopyRect(&rect, &this->rect);
		rect.left -= TEXT_RECTANGLE_ADD;
		rect.top -= TEXT_RECTANGLE_ADD;
		rect.right += TEXT_RECTANGLE_ADD;
		rect.bottom += TEXT_RECTANGLE_ADD;
		if (PtInRect(&rect, pt))
			return true;
		return false;
	}
	if (PtInRect(&rect, pt))
		return true;
	return false;
}

bool TextObj::isClickToSelectEnhanced(int xClient, int yClient)
{
	return TextObj::isClickToSelectNormal(xClient, yClient);
}

void TextObj::move(int oldXClient, int oldYClient, int xClient, int yClient)
{
	int deltaX = xClient - oldXClient;
	int deltaY = yClient - oldYClient;
	this->rect.left += deltaX;
	this->rect.top += deltaY;
	this->rect.right += deltaX;
	this->rect.bottom += deltaY;
}

void TextObj::setBeingSelected(bool flag)
{
	this->isBeingSelected = flag;
}

void TextObj::setColor(COLORREF color)
{
	this->color = color;
}

void TextObj::edit(HDC hdc,int oldXClient, int oldYClient, int xClient, int yClient)
{
	
	int deltaX = xClient - oldXClient;
	int deltaY = yClient - oldYClient;
	if (deltaY == 0)
		return;
	if (this->_recentCorner == CORNER_LT)
	{
		OutputDebugStringA("Left Top Corner\n");
		if (this->rect.left + deltaX > this->rect.right || this->rect.top + deltaY > this->rect.bottom )
			return;
		if (this->_font.lfHeight + deltaY >= 0)
			return;
		
		this->_font.lfHeight += deltaY;
		rect.left += deltaX;
		rect.top += deltaY;
	}
	else if (this->_recentCorner == CORNER_LB)
	{
		OutputDebugStringA("Left Bot Corner\n");

		if (this->rect.left + deltaX > this->rect.right || this->rect.bottom + deltaY < this->rect.top)
			return;
		if (this->_font.lfHeight - deltaY >= 0)
			return;
		this->_font.lfHeight -= deltaY;
		rect.left += deltaX;
		rect.bottom += deltaY;
	}
	else if (this->_recentCorner == CORNER_RT)
	{
		OutputDebugStringA("Right Top Corner\n");

		if (this->rect.right + deltaX < this->rect.left || this->rect.top + deltaY > this->rect.bottom)
			return;
		if (this->_font.lfHeight + deltaY >= 0)
			return;
		this->_font.lfHeight += deltaY;
		rect.top += deltaY;
		rect.right += deltaX;
	}
	else if (this->_recentCorner == CORNER_RB)
	{
		OutputDebugStringA("Right Bot Corner\n");

		if (this->rect.right + deltaX < this->rect.left || this->rect.bottom + deltaY < this->rect.top)
			return;
		if (this->_font.lfHeight - deltaY >= 0)
			return;
		this->_font.lfHeight -= deltaY;
		rect.bottom += deltaY;
		rect.right += deltaX;
	}
	DeleteObject(this->_hFont);
	_hFont = CreateFontIndirect(&this->_font);
}

bool TextObj::isClickToEdit(int xClient, int yClient)
{
	RECT smallRect;
	POINT ptClick;
	ptClick.x = xClient;
	ptClick.y = yClient;
	setBeingSelected(true);
	//Initialize left top corner rect
	POINT pt;
	pt.x = rect.left - TEXT_RECTANGLE_ADD;
	pt.y = rect.top - TEXT_RECTANGLE_ADD;
	fillRectAroundPoint(&smallRect, pt, FOCUS_SQUARE_SIZE);
	if (PtInRect(&smallRect, ptClick))
	{
		this->_recentCorner = CORNER_LT;
		return true;
	}
	//Initialize right top corner rect
	pt.x = rect.right + TEXT_RECTANGLE_ADD;
	pt.y = rect.top - TEXT_RECTANGLE_ADD;
	fillRectAroundPoint(&smallRect, pt, FOCUS_SQUARE_SIZE);
	if (PtInRect(&smallRect, ptClick))
	{
		this->_recentCorner = CORNER_RT;
		return true;
	}
	//Initilize left bottom corner rect
	pt.x = rect.left - TEXT_RECTANGLE_ADD;
	pt.y = rect.bottom + TEXT_RECTANGLE_ADD;
	fillRectAroundPoint(&smallRect, pt, FOCUS_SQUARE_SIZE);
	if (PtInRect(&smallRect, ptClick))
	{
		this->_recentCorner = CORNER_LB;
		return true;
	}
	//Initialize right bottom corner rect
	pt.x = rect.right + TEXT_RECTANGLE_ADD;
	pt.y = rect.bottom + TEXT_RECTANGLE_ADD;
	fillRectAroundPoint(&smallRect, pt, FOCUS_SQUARE_SIZE);
	if (PtInRect(&smallRect, ptClick))
	{
		this->_recentCorner = CORNER_RB;
		return true;
	}
	return false;
}

void TextObj::drawSelectedStyle(HDC hdc)
{
	RECT styleRect;
	styleRect.left = rect.left - TEXT_RECTANGLE_ADD;
	styleRect.top = rect.top - TEXT_RECTANGLE_ADD;
	styleRect.right = rect.right + TEXT_RECTANGLE_ADD;
	styleRect.bottom = rect.bottom + TEXT_RECTANGLE_ADD;
	SelectObject(hdc, GetStockObject(HOLLOW_BRUSH));
	RECT lftpRect;
	RECT rgtpRect;
	RECT lfbtRect;
	RECT rgbtRect;

	//Initialize left top corner rect
	POINT pt;
	pt.x = rect.left - TEXT_RECTANGLE_ADD;
	pt.y = rect.top - TEXT_RECTANGLE_ADD;
	fillRectAroundPoint(&lftpRect, pt, FOCUS_SQUARE_SIZE);
	//Initialize right top corner rect
	pt.x = rect.right + TEXT_RECTANGLE_ADD;
	pt.y = rect.top - TEXT_RECTANGLE_ADD;
	fillRectAroundPoint(&rgtpRect, pt, FOCUS_SQUARE_SIZE);
	//Initilize left bottom corner rect
	pt.x = rect.left - TEXT_RECTANGLE_ADD;
	pt.y = rect.bottom + TEXT_RECTANGLE_ADD;
	fillRectAroundPoint(&lfbtRect, pt, FOCUS_SQUARE_SIZE);
	//Initialize right bottom corner rect
	pt.x = rect.right + TEXT_RECTANGLE_ADD;
	pt.y = rect.bottom+ TEXT_RECTANGLE_ADD;
	fillRectAroundPoint(&rgbtRect, pt, FOCUS_SQUARE_SIZE);
	//Draw the squares
	Rectangle(hdc, lftpRect.left, lftpRect.top, lftpRect.right, lftpRect.bottom);
	Rectangle(hdc, lfbtRect.left, lfbtRect.top, lfbtRect.right, lfbtRect.bottom);
	Rectangle(hdc, rgtpRect.left, rgtpRect.top, rgtpRect.right, rgtpRect.bottom);
	Rectangle(hdc, rgbtRect.left, rgbtRect.top, rgbtRect.right, rgbtRect.bottom);
	DrawFocusRect(hdc, &styleRect);
}

void TextObj::setData(int x1, int y1, int x2, int y2)
{
	this->rect.left = this->rect.right = x1;
	this->rect.top = this->rect.bottom = y1;
}

void TextObj::setText(const std::wstring & newContent)
{
	this->_content = newContent;
}

void TextObj::setText(std::wstring && newContent)
{
	this->_content = newContent;
}

void TextObj::setFont(const LOGFONT & font)
{
	this->_font = font;
	DeleteObject(this->_hFont);
	this->_hFont = CreateFontIndirect(&this->_font);
}

void TextObj::setAnchor(int x, int y)
{
	rect.right = 0;
	rect.bottom = 0;
	rect.left = x;
	rect.top = y;
}

LOGFONT TextObj::getFont()
{
	return this->_font;
}

const std::wstring & TextObj::getText()
{
	return this->_content;
	// TODO: insert return statement here
}

bool TextObj::saveToFile(HANDLE hFile)
{
	if (!WriteFile(hFile, &this->type, sizeof(UINT), 0, 0))
		return false;
	if (!WriteFile(hFile, &this->color, sizeof(COLORREF), 0, 0))
		return false;
	if (!WriteFile(hFile, &this->_font, sizeof(LOGFONT), 0, 0))
		return false;
	if (!WriteFile(hFile, &this->rect.left, sizeof(int), 0, 0))
		return false;
	if (!WriteFile(hFile, &this->rect.top, sizeof(int), 0, 0))
		return false;
	auto size = this->_content.size();
	if (!WriteFile(hFile, &size, sizeof(UINT), 0, 0))
		return false;
	if (!WriteFile(hFile, this->_content.c_str(), sizeof(WCHAR)*(this->_content.size() + 1), 0, 0))
		return false;
	return true;
}

bool TextObj::loadFromFile(HANDLE hFile)
{
	if (!ReadFile(hFile, &this->color, sizeof(COLORREF), 0, 0))
		return false;
	LOGFONT font;
	if (!ReadFile(hFile, &font, sizeof(LOGFONT), 0, 0))
		return false;
	TextObj::setFont(font);
	if (!ReadFile(hFile, &this->rect.left, sizeof(int), 0, 0))
		return false;
	if (!ReadFile(hFile, &this->rect.top, sizeof(int), 0, 0))
		return false;
	TCHAR *buffer;
	UINT size{};
	if (!ReadFile(hFile, &size, sizeof(UINT), 0, 0))
		return false;
	buffer = new TCHAR[size + 1];
	if (!ReadFile(hFile, buffer, sizeof(WCHAR)*(size + 1), 0, 0))
	{
		delete[] buffer;
		return false;
	}
	this->_content.assign(std::wstring(buffer));
	delete[] buffer;
	return true;
}


Shape * TextObj::cloneRetPtr()
{
	auto copObj = new TextObj();
	((TextObj*)copObj)->color = this->color;
	std::random_device rd;
	std::mt19937 mt(rd());
	std::uniform_real_distribution<double> gen(-3,3);
	auto width = this->rect.right - this->rect.left;
	auto height = this->rect.bottom - this->rect.top;
	auto delta = gen(mt)*PADDING_CC;
	auto delta1 = gen(mt)*PADDING_CC;
	((TextObj*)copObj)->rect.left = this->rect.left +  delta;
	((TextObj*)copObj)->rect.top = this->rect.top + delta1;
	((TextObj*)copObj)->rect.right = ((TextObj*)copObj)->rect.left + width;
	((TextObj*)copObj)->rect.bottom = ((TextObj*)copObj)->rect.top + height;
	((TextObj*)copObj)->setText(this->getText());
	((TextObj*)copObj)->setFont(this->getFont());;
	return copObj;
}

UINT TextObj::size()
{
	return sizeof(TextObj);
}
