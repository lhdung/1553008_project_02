//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by MyApps.rc
//
#define IDC_MYICON                      2
#define DLG_COLOR                       10
#define NUM_CUSTOM_COLORS               16
#define NUM_BASIC_COLORS                48
#define IDD_MYAPPS_DIALOG               102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDS_TOOLTIP_FILE_NEW            104
#define IDM_EXIT                        105
#define IDS_TOOLTIP_FILE_OPEN           105
#define IDS_EDIT_LABEL                  106
#define IDI_MYAPPS                      107
#define IDS_TOOLTIP_FILE_SAVE           107
#define IDI_SMALL                       108
#define IDS_TOOLTIP_DRAW_LINE           108
#define IDC_MYAPPS                      109
#define ID_TOOLBAR                      110
#define IDS_TOOLTIP_DRAW_RECTANGLE      110
#define IDM_FILE_NEW                    111
#define IDS_TOOLTIP_DRAW_ELLIPSE        111
#define IDM_FILE_OPEN                   112
#define IDS_TOOLTIP_DRAW_TEXT           112
#define IDM_FILE_SAVE                   113
#define IDS_TOOLTIP_DRAWMODE            113
#define IDM_DRAW_COLOR                  114
#define IDS_TOOLTIP_SELECTIONMODE       114
#define IDM_DRAW_FONT                   115
#define IDM_DRAW_LINE                   116
#define IDM_DRAW_RECTANGLE              117
#define IDM_DRAW_ELLIPSE                118
#define IDM_DRAW_TEXT                   119
#define IDM_WINDOW_TILE                 120
#define IDM_WINDOW_CASCADE              121
#define IDM_WINDOW_CLOSEALL             122
#define IDB_DRAWINGMODE                 123
#define IDB_EDITMODE                    124
#define IDR_MAINFRAME                   128
#define IDD_INPUTTEXT                   129
#define COLOR_HUESCROLL                 700
#define COLOR_SATSCROLL                 701
#define COLOR_LUMSCROLL                 702
#define COLOR_HUE                       703
#define COLOR_SAT                       704
#define COLOR_LUM                       705
#define COLOR_RED                       706
#define COLOR_GREEN                     707
#define COLOR_BLUE                      708
#define COLOR_CURRENT                   709
#define COLOR_RAINBOW                   710
#define COLOR_SAVE                      711
#define COLOR_ADD                       712
#define COLOR_SOLID                     713
#define COLOR_TUNE                      714
#define COLOR_SCHEMES                   715
#define COLOR_ELEMENT                   716
#define COLOR_SAMPLES                   717
#define COLOR_PALETTE                   718
#define COLOR_MIX                       719
#define COLOR_BOX1                      720
#define COLOR_CUSTOM1                   721
#define COLOR_HUEACCEL                  723
#define COLOR_SATACCEL                  724
#define IDD_COLORBOX                    724
#define COLOR_LUMACCEL                  725
#define COLOR_REDACCEL                  726
#define COLOR_GREENACCEL                727
#define COLOR_BLUEACCEL                 728
#define COLOR_SOLID_LEFT                730
#define COLOR_SOLID_RIGHT               731
#define IDC_EDIT1                       1000
#define IDC_TEXT                        1000
#define ID_EDIT_DRAWMODE                32788
#define ID_EDIT_SELECTIONMODE           32789
#define ID_EDIT_CUT                     32790
#define ID_EDIT_COPY                    32791
#define ID_EDIT_PASTE                   32792
#define ID_EDIT_DELETE                  32793
#define ID_ACCELERATOR32799             32799
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32801
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
