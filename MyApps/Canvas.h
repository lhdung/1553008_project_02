#pragma once
#include "stdafx.h"
#include "Shape.h"
class Canvas
{
public:
	/*------------Get Method-------------*/
	UINT getDrawMode();
	LOGFONT getFont();
	COLORREF getDrawColor();
	UINT getEditMode();
	Shape* getSelectedObject();
	Shape* getLatestObject(); //Get the object that is most recently added to the canvas
	HANDLE getFileHandle(); 
	/*-----------Set Method--------------*/
	void setDrawColor(const COLORREF& color);
	void setFont(const LOGFONT& font);
	void setDrawMode(const UINT drawMode);
	void setFileHandle(HANDLE file);
	void setEditMode(UINT editMode);
	void remove(Shape* shape);
	/*-----------Draw Method-------------*/
	void draw(HDC hdc);
	/*-----------Manipulating objects method----------*/
	bool addNewObject(Shape* shape);

	//move the selected object to the new position
	void moveSelectObject(int xClient, int yClient); 

	//edit the selected object, meaning that setting new data for objects
	void editSelectedObject(int xClient, int yClient); 
	//this function will find the object that contains the click and that object becomes selected
	//the newer object has higher priority to be selected than older object
	//if no object contains the click, deselect the selected object if any.
	UINT setSelectObject(int xClient, int yClient);

	//to deselect the selected object.
	BOOL deselSelectObject();

	/*-----------Init and Free resource method------------*/
	void freeResource();
	void initResource();
	/*-----------File manipulating method-----------------*/
	bool saveToFile();
	bool loadDataFromFile();
	
	//constructor and destructor
	Canvas(HWND hWnd) : _drawColor{}, _font{}, _drawMode{}, _editMode{}, _selectedObj(NULL), _xRecentClick(0), _yRecentClick(0), _isClickToMoveObject(false), _isClickToEditSelectObject(false),
		_isClickToDrawObject(false), _isObjectCreated(false), _isMaximized(false), fileHandle{}
	{
		_hWnd = hWnd;
	}
private:
	COLORREF _drawColor{}; //Current color of the canvas
	LOGFONT _font{}; //Current font of the canvas
	HANDLE fileHandle{};//The file, canvas associates with
	UINT _drawMode{}; //The draw mode of the canvas: Rectangle, Ellipse, Line ,Text
	UINT _editMode{};//The edit mode of the canvas: drawing mode, selection mode
	Shape* _selectedObj; //The pointer to the current selected object, this pointer is null if no object is selected
	std::vector<Shape*> _shapeArr; //The array of all objects currently in the canvas
public:
	HWND _hWnd; //The window that canvas associates with
	int _xRecentClick; //The xClick that canvas keep track to work with drawing objects
	int _yRecentClick; //The yClick that canvas keep track to work with drawing objects
	bool _isClickToMoveObject; //The flag to indicates the user want to move the objects
	bool _isClickToEditSelectObject; //The flag to indicates the user want to edit the objects
	bool _isClickToDrawObject; //The flag to indicates the user want to draw new objects
	bool _isObjectCreated; //The flag to indicates the objects is already created, so we just set its data rather than create the new one
	bool _isMaximized; //The flag to indicates the window is maximized to behave correctly when checking DrawMode and EditMode in Menu
	bool _isSaved;
	bool _timeCopied{};
	bool _isCopy{};
};