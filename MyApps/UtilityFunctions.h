#pragma once
#include "stdafx.h"
struct LineEquation
{
	int aCoef;
	int bCoef;
	int cConst;
};
void normalizeRect(RECT *rectPtr);
void fillRectAroundPoint(RECT* square, POINT pt, UINT width);
LineEquation constructLineEquation(const POINT& pt1, const POINT& pt2);
double distanceToLine(POINT& pt, LineEquation& line);
POINT findProjectionPoint(const POINT& pt, const LineEquation& line);
POINT solveTwoEquations(const LineEquation& line1, const LineEquation& line2);
double length(const POINT& pt1, const POINT& pt2);