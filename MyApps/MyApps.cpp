﻿// MyApps.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "MyApps.h"
#include <CommCtrl.h>
#include <commdlg.h>
#include "DrawWindow.h"
#include <ColorDlg.h>
#include "AppsDef.h"
#define MAX_LOADSTRING 100
#define IMAGE_WIDTH 16
#define IMAGE_HEIGHT 16
#define WINDOW_ID_POS 2
// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
HWND hMDIClient = NULL;
HWND framWnd;
UINT iFormat;
// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
HWND CreateToolbar(HWND hWnd);
HWND CreateMDIClient(HWND hWnd);
BOOL CALLBACK CloseChildProc(HWND hWnd, LPARAM lParam);
int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: Place code here.
	//Register Format
	iFormat = RegisterClipboardFormatA("PAINTOBJ");
	// Initialize global strings
	LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadStringW(hInstance, IDC_MYAPPS, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);
	RegisterDrawWindow(hInstance);
	// Perform application initialization:
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}

	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MYAPPS));

	MSG msg;

	// Main message loop:
	while (GetMessage(&msg, nullptr, 0, 0))
	{
		if (!TranslateMDISysAccel(hMDIClient, &msg) && !TranslateAccelerator(framWnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int)msg.wParam;
}

//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MYAPPS));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_MYAPPS);
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	hInst = hInstance; // Store instance handle in our global variable

	framWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

	if (!framWnd)
	{
		return FALSE;
	}

	ShowWindow(framWnd, nCmdShow);
	UpdateWindow(framWnd);

	return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HWND hToolbar = NULL;
	static int numOfWindows = 0;
	//SetFocus(hWnd);
	switch (message)
	{
	case WM_COMMAND:
	{
		int wmId = LOWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_FILE_NEW:
		{
			HWND childhWnd;
			childhWnd = CreateChildWindow(hMDIClient, numOfWindows, INVALID_HANDLE_VALUE,NULL);
			if (childhWnd)
			{
				ShowWindow(childhWnd, SW_SHOW);
				numOfWindows++;
			}
			break;
		}
		case IDM_FILE_OPEN:
		{
			OnOpen(hWnd, wParam, lParam, hMDIClient, numOfWindows);
			break;
		}
		case IDM_FILE_SAVE:
		case ID_EDIT_COPY:
		case ID_EDIT_CUT:
		case ID_EDIT_PASTE:
		case ID_EDIT_DELETE:
		{
			HWND childWnd = (HWND)SendMessage(hMDIClient, WM_MDIGETACTIVE, 0, 0);
			if (childWnd != NULL)
			{
				SendMessage(childWnd, message, wParam, lParam);
			}
		}
		break;
		case IDM_WINDOW_CASCADE:
			SendMessage(hMDIClient, WM_MDICASCADE, 0, 0);
			break;
		case IDM_WINDOW_TILE:
			SendMessage(hMDIClient, WM_MDITILE, MDITILE_SKIPDISABLED, 0);
			break;
		case IDM_WINDOW_CLOSEALL:
			EnumChildWindows(hMDIClient, CloseChildProc, lParam);
			break;
		case IDM_DRAW_COLOR:
		{
			HWND childWnd = (HWND)SendMessage(hMDIClient, WM_MDIGETACTIVE, 0, 0);
			COLORREF colorArr[16];
			CHOOSECOLOR cc;
			ZeroMemory(&cc, sizeof(CHOOSECOLOR));
			cc.lStructSize = sizeof(CHOOSECOLOR);
			cc.hInstance = (HWND)hInst;
			cc.hwndOwner = (HWND)SendMessage(hMDIClient, WM_MDIGETACTIVE, 0, 0);
			cc.Flags = CC_RGBINIT;
			if (childWnd != NULL)
				cc.rgbResult = GetWindowData(childWnd)->getDrawColor();
			else
				cc.rgbResult = RGB(0, 0, 0);
			cc.lpCustColors = (LPDWORD)colorArr;
			if (ChooseColor(&cc))
			{
				if (childWnd != NULL)
					GetWindowData(childWnd)->setDrawColor(cc.rgbResult);
			}
			break;
		}
		case IDM_DRAW_FONT:
		{
			HWND childWnd = (HWND)SendMessage(hMDIClient, WM_MDIGETACTIVE, 0, 0);
			CHOOSEFONT cf{};
			LOGFONT lgFont{};
			if (childWnd != NULL)
				lgFont = GetWindowData(childWnd)->getFont();
			cf.lStructSize = sizeof(CHOOSEFONT);
			cf.hwndOwner = hWnd;
			cf.lpLogFont = &lgFont;
			cf.Flags = CF_EFFECTS | CF_INITTOLOGFONTSTRUCT;
			if (childWnd != NULL)
				cf.rgbColors = GetWindowData(childWnd)->getDrawColor();
			else
				cf.rgbColors = RGB(0, 0, 0);
			cf.hDC = NULL;
			if (ChooseFont(&cf))
			{
				if (childWnd != NULL)
				{
					auto canvas = GetWindowData(childWnd);
					canvas->setFont(*cf.lpLogFont);
				}
			}
			break;
		}
		case IDM_DRAW_LINE:
		case IDM_DRAW_TEXT:
		case IDM_DRAW_ELLIPSE:
		case IDM_DRAW_RECTANGLE:
		case ID_EDIT_DRAWMODE:
		case ID_EDIT_SELECTIONMODE:
		{
			HWND childMDI = (HWND)SendMessage(hMDIClient, WM_MDIGETACTIVE, 0, 0);
			if (childMDI != NULL)
			{
				PostMessage(childMDI, WM_COMMAND, wmId, 0);
			}
			break;
		}
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefFrameProc(hWnd, hMDIClient, message, wParam, lParam);
		}
	}
	return DefFrameProc(hWnd, hMDIClient, message, wParam, lParam);
	case WM_NOTIFY:
		switch (((LPNMHDR)lParam)->code)
		{
		case TTN_GETDISPINFO:
		{
			LPTOOLTIPTEXT lpDispInfo = (LPTOOLTIPTEXT)lParam;
			UINT id = lpDispInfo->hdr.idFrom;
			switch (id)
			{
			case IDM_FILE_NEW:
				lpDispInfo->lpszText = MAKEINTRESOURCE(IDS_TOOLTIP_FILE_NEW);
				break;
			case IDM_FILE_OPEN:
				lpDispInfo->lpszText = MAKEINTRESOURCE(IDS_TOOLTIP_FILE_OPEN);
				break;
			case IDM_FILE_SAVE:
				lpDispInfo->lpszText = MAKEINTRESOURCE(IDS_TOOLTIP_FILE_SAVE);
				break;
			case IDM_DRAW_LINE:
				lpDispInfo->lpszText = MAKEINTRESOURCE(IDS_TOOLTIP_DRAW_LINE);
				break;
			case IDM_DRAW_RECTANGLE:
				lpDispInfo->lpszText = MAKEINTRESOURCE(IDS_TOOLTIP_DRAW_RECTANGLE);
				break;
			case IDM_DRAW_ELLIPSE:
				lpDispInfo->lpszText = MAKEINTRESOURCE(IDS_TOOLTIP_DRAW_ELLIPSE);
				break;
			case IDM_DRAW_TEXT:
				lpDispInfo->lpszText = MAKEINTRESOURCE(IDS_TOOLTIP_DRAW_TEXT);
				break;
			case ID_EDIT_SELECTIONMODE:
				lpDispInfo->lpszText = MAKEINTRESOURCE(IDS_TOOLTIP_SELECTIONMODE);
				break;
			case ID_EDIT_DRAWMODE:
				lpDispInfo->lpszText = MAKEINTRESOURCE(IDS_TOOLTIP_DRAWMODE);
				break;
			default:
				break;
			}
		}
		default:
			break;
		}
		break;
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code that uses hdc here...
		EndPaint(hWnd, &ps);
	}
	break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	case WM_CREATE:
	{
		SetFocus(hWnd);
		//Create Toolbar
		hToolbar = CreateToolbar(hWnd);
		if (hToolbar == NULL)
			return -1;
		//Create MDI window
		hMDIClient = CreateMDIClient(hWnd);
		if (hMDIClient == NULL)
			return -1;
		CheckMenuRadioItem(GetSubMenu(GetMenu(hWnd), DRAWMENU_POS), DRAWMODE_LINE, DRAWMODE_TEXT, DRAWMODE_LINE, MF_BYCOMMAND);
		CheckMenuRadioItem(GetSubMenu(GetMenu(hWnd), EDITMENU_POS), EDITMODE_DRAW, EDITMODE_SELECT, EDITMODE_DRAW, MF_BYCOMMAND);

		return 0;
	}
	case WM_GETTOOLBAR:
		return(LRESULT) hToolbar;
	case WM_SIZE:
	{
		//Resize the MDIClient according to the height of the Toolbar
		DWORD buttonMetrics = SendMessage(hToolbar, TB_GETBUTTONSIZE, 0, 0);
		DWORD padding = SendMessage(hToolbar, TB_GETPADDING, 0, 0);
		UINT w = LOWORD(lParam);
		UINT h = HIWORD(lParam);
		MoveWindow(hMDIClient, 0, 0 + HIWORD(buttonMetrics) + HIWORD(padding), w, h, TRUE);
		MoveWindow(hToolbar, 0, 0, w, h, TRUE);
		return 0;
	}
	default:
		return DefFrameProc(hWnd, hMDIClient, message, wParam, lParam);
	}
	//SetFocus(hWnd);
	return DefFrameProc(hWnd, hMDIClient, message, wParam, lParam);
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

HWND CreateToolbar(HWND hWnd)
{
	//Init Toolbar
	INITCOMMONCONTROLSEX InitCmCtrlStruct;
	InitCmCtrlStruct.dwSize = sizeof(INITCOMMONCONTROLSEX);
	InitCmCtrlStruct.dwICC = ICC_BAR_CLASSES;
	InitCommonControlsEx(&InitCmCtrlStruct);
	HWND hToolbar = CreateWindowEx(0, TOOLBARCLASSNAME, 0,
		WS_CHILD | CCS_ADJUSTABLE | WS_VISIBLE | TBSTYLE_TOOLTIPS | TBSTYLE_WRAPABLE, 0, 0, 0, 0, hWnd, (HMENU)ID_TOOLBAR, hInst, NULL);
	if (!hToolbar)
		return NULL;
	//Add Standar Button to Toolbar
	HIMAGELIST hImgList = ImageList_Create(IMAGE_WIDTH, IMAGE_HEIGHT, ILC_COLOR, 0, 0);
	SendMessage(hToolbar, TB_SETIMAGELIST, 0, (LPARAM)hImgList);
	SendMessage(hToolbar, TB_LOADIMAGES, IDB_STD_SMALL_COLOR, (LPARAM)HINST_COMMCTRL);
	//Load Bitmap for drawing mode icon
	//TBADDBITMAP tbAddBmp = { hInst,IDB_DRAWINGMODE };
	//int id = SendMessage(hToolbar, TB_ADDBITMAP, sizeof(tbAddBmp) / sizeof(TBADDBITMAP), (LPARAM)(LPTBADDBITMAP)&tbAddBmp);
	HBITMAP bmp = LoadBitmap(hInst, MAKEINTRESOURCE(IDB_DRAWINGMODE));
	HBITMAP bmp2 = LoadBitmap(hInst, MAKEINTRESOURCE(IDB_EDITMODE));
	int id = ImageList_Add(hImgList, bmp, NULL);
	int id2 = ImageList_Add(hImgList, bmp2, NULL);
	//Create Button and Add to toolbar
	SendMessage(hToolbar, TB_BUTTONSTRUCTSIZE, (WPARAM)sizeof(TBBUTTON), 0);
	//int iNew = SendMessage(hToolbar, TB_ADDSTRING,(WPARAM)hInst,(LPARAM) IDS_EDIT_LABEL);
	TBBUTTON buttonStdArr[] = {
		{ MAKELONG(STD_FILENEW,0),IDM_FILE_NEW,TBSTATE_ENABLED,TBSTYLE_BUTTON,{0},0/*,iNew*/ },
		{ MAKELONG(STD_FILEOPEN,0),IDM_FILE_OPEN,TBSTATE_ENABLED,TBSTYLE_BUTTON,{0},0/*,iNew + 1*/ },
		{ MAKELONG(STD_FILESAVE,0),IDM_FILE_SAVE,TBSTATE_ENABLED,TBSTYLE_BUTTON,{0},0/*,iNew + 2*/ },
	};
	SendMessage(hToolbar, TB_ADDBUTTONS, sizeof(buttonStdArr) / sizeof(TBBUTTON), (LPARAM)&buttonStdArr);
	//Add copy,paste,cut,delete button to toolbar
	TBBUTTON buttonEditArr[] = {
		{ 0,IDM_FILE_NEW,TBSTATE_ENABLED,TBSTYLE_SEP,{ 0 },0/*,iNew*/ },
		{ MAKELONG(STD_COPY,0),ID_EDIT_COPY,TBSTATE_ENABLED,TBSTYLE_BUTTON,{ 0 },0/*,iNew*/ },
		{ MAKELONG(STD_CUT,0),ID_EDIT_CUT,TBSTATE_ENABLED,TBSTYLE_BUTTON,{ 0 },0/*,iNew*/ },
		{ MAKELONG(STD_PASTE,0),ID_EDIT_PASTE,TBSTATE_ENABLED,TBSTYLE_BUTTON,{ 0 },0/*,iNew*/ },
		{ MAKELONG(STD_DELETE,0),ID_EDIT_DELETE,TBSTATE_ENABLED,TBSTYLE_BUTTON,{ 0 },0/*,iNew*/ },
	};
	SendMessage(hToolbar, TB_ADDBUTTONS, sizeof(buttonEditArr) / sizeof(TBBUTTON), (LPARAM)&buttonEditArr);
	//Add button to manipulate painting object
	TBBUTTON buttonDrawArr[] =
	{
		{ 0, 0 , TBSTATE_ENABLED, TBSTYLE_SEP,0,0},
		{ id + 0,IDM_DRAW_LINE,TBSTATE_ENABLED,TBSTYLE_BUTTON | BTNS_CHECKGROUP,{0},0/*,iNew + 3*/ },
		{ id + 1,IDM_DRAW_RECTANGLE,TBSTATE_ENABLED,TBSTYLE_BUTTON |  BTNS_CHECKGROUP,{0},0 /*,iNew + 4*/ },
		{ id + 2,IDM_DRAW_ELLIPSE,TBSTATE_ENABLED,TBSTYLE_BUTTON | BTNS_CHECKGROUP,{0},0/*,iNew + 5*/ },
		{ id + 3,IDM_DRAW_TEXT,TBSTATE_ENABLED,TBSTYLE_BUTTON | BTNS_CHECKGROUP ,{0},0 /*,iNew + 6*/ },
		{0,0,TBSTATE_ENABLED,TBSTYLE_SEP,0,0},
		{id2 + 0 , ID_EDIT_DRAWMODE , TBSTATE_ENABLED , TBSTYLE_BUTTON | BTNS_CHECKGROUP ,{0},0},
		{ id2 + 1 , ID_EDIT_SELECTIONMODE , TBSTATE_ENABLED , TBSTYLE_BUTTON | BTNS_CHECKGROUP,{ 0 },0 }
	};
	SendMessage(hToolbar, TB_ADDBUTTONS, sizeof(buttonDrawArr) / sizeof(TBBUTTON), (LPARAM)&buttonDrawArr);
	SendMessage(hToolbar, TB_AUTOSIZE, 0, 0);
	ShowWindow(hToolbar, SW_SHOW);
	return hToolbar;
}

HWND CreateMDIClient(HWND hWnd)
{
	CLIENTCREATESTRUCT clientCrStr;
	clientCrStr.hWindowMenu = GetSubMenu(GetMenu(hWnd), WINDOW_ID_POS);
	clientCrStr.idFirstChild = 0;
	hMDIClient = CreateWindow(L"MDICLIENT", L"MDI Client",
		WS_CHILD | WS_OVERLAPPED| WS_CLIPCHILDREN | WS_VISIBLE | WS_VSCROLL | WS_HSCROLL,
		0, 0, 0, 0, hWnd, NULL, hInst, (LPCLIENTCREATESTRUCT)&clientCrStr);
	if (hMDIClient == NULL)
		return NULL;
	return hMDIClient;
}

BOOL CALLBACK CloseChildProc(HWND hWnd, LPARAM lParam)
{
	SendMessage(hMDIClient, WM_MDIDESTROY, (WPARAM)hWnd, lParam);
	return TRUE;
}