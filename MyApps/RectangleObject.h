#pragma once
#include "Shape.h"
#include "Resource.h"
#include "AppsDef.h"
class Canvas;
class RectObj : public Shape
{
public:
	//---------Class Method--------------
	RectObj(int color);
	RectObj();
	~RectObj();
	void draw(HDC hdc);
	bool isClickToSelect(int xClient, int yClient);
	bool isClickToSelectNormal(int xClient, int yClient);
	bool isClickToSelectEnhanced(int xClient, int yClient);
	void move(int oldXClient, int oldYClient, int xClient, int yClient);
	void drawSelectedStyle(HDC hdc);
	void setBeingSelected(bool flag);
	void setColor(COLORREF color);
	bool isClickToEdit(int xClient, int yClient);
	void edit(HDC hdc,int oldXClient, int oldYClient, int xClient, int yClient);
	void setData(int x1, int y1, int x2, int y2);
	void setAnchor(int x, int y);
	bool saveToFile(HANDLE hFile);
	bool loadFromFile(HANDLE hFile);
	Shape* cloneRetPtr();
	UINT size();
	//----------Friend Class---------
	friend class Canvas;
private:
	//---------Class Member--------------
	RECT rect; //Real Rectangle
	UINT _recentCorner; //corner user click to edit
	HPEN _pen;
};