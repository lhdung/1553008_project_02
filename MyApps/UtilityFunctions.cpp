#include "stdafx.h"
#include "UtilityFunctions.h"
#include "Resource.h"
void normalizeRect(RECT * rectPtr)
{
	if (rectPtr == NULL)
		return;
	if (rectPtr->left < rectPtr->right && rectPtr->top < rectPtr->bottom)
		return;
	if (rectPtr->left < rectPtr->right)
	{
		if (rectPtr->top > rectPtr->bottom)
		{
			std::swap(rectPtr->top, rectPtr->bottom);
		}
	}
	else if (rectPtr->left > rectPtr->right)
	{
		if (rectPtr->top < rectPtr->bottom)
		{
			std::swap(rectPtr->left, rectPtr->right);
		}
		else if (rectPtr->top > rectPtr->bottom)
		{
			std::swap(rectPtr->left, rectPtr->right);
			std::swap(rectPtr->top, rectPtr->bottom);
		}
	}
}
void fillRectAroundPoint(RECT* square, POINT pt, UINT width)
{
	square->left = pt.x - width;
	square->right = pt.x + width;
	square->top = pt.y - width;
	square->bottom = pt.y + width;
}

LineEquation constructLineEquation(const POINT & pt1, const POINT & pt2)
{
	LineEquation line;
	int xVector = pt1.x - pt2.x;
	int yVector = pt1.y - pt2.y;
	std::swap(xVector, yVector);
	xVector = -xVector;
	line.aCoef = xVector;
	line.bCoef = yVector;
	line.cConst = line.aCoef*(-pt1.x) + line.bCoef*(-pt1.y);
	return line;
}

double distanceToLine(POINT & pt, LineEquation & line)
{
	double result = pow(abs(line.aCoef*pt.x + line.bCoef*pt.y + line.cConst), 2) / (pow(line.aCoef, 2) + pow(line.bCoef, 2));
	return sqrt(result);
}

POINT findProjectionPoint(const POINT & pt, const LineEquation & line)
{
	LineEquation line2;
	line2.aCoef = -line.bCoef;
	line2.bCoef = line.aCoef;
	line2.cConst = line.bCoef*pt.x - line.aCoef*pt.y;
	return solveTwoEquations(line, line2);
}

POINT solveTwoEquations(const LineEquation & line1, const LineEquation & line2)
{
	POINT pt;
	pt.y = (line2.aCoef*line1.cConst - line1.aCoef*line2.cConst) / (line1.aCoef*line2.bCoef - line2.aCoef*line1.bCoef);
	pt.x = (-line1.cConst - line1.bCoef*pt.y) / line1.aCoef;
	return pt;
}

double length(const POINT & pt1, const POINT & pt2)
{
	long long a = pow(pt1.x - pt2.x, 2);
	long long b = pow(pt1.y - pt2.y, 2);
	return sqrtl(a + b);
}