#include "stdafx.h"
#include "EllipseObject.h"
#include "Resource.h"
#include "UtilityFunctions.h"
#include "AppsDef.h"
EllipseObj::EllipseObj(int color) : Shape(color)
{
	this->type = ELLIPSE;
	this->rect = {};
	this->_recentCorner = {};
	this->_pen = NULL;
}
EllipseObj::EllipseObj()
{
	this->type = ELLIPSE;
	this->rect = {};
	this->_recentCorner = {};
	this->_pen = NULL;
}
EllipseObj::~EllipseObj()
{
	DeleteObject(this->_pen);
}
void EllipseObj::draw(HDC hdc)
{

	//Select it into HDC and keep the current Pen of the HDC
	HPEN curPen = NULL;
	HPEN selectSquarePen = NULL;
	if (isBeingSelected)
	{
		selectSquarePen = CreatePen(PS_SOLID, FOCUS_SQUARE_PEN_WIDTH, RGB(45, 114, 178));
		curPen = (HPEN)SelectObject(hdc, selectSquarePen);
		drawSelectedStyle(hdc);
		SelectObject(hdc, this->_pen);
		Ellipse(hdc, rect.left, rect.top, rect.right, rect.bottom);
	}
	else
	{
		curPen = (HPEN)SelectObject(hdc, this->_pen);
		Ellipse(hdc, rect.left, rect.top, rect.right, rect.bottom);
	}
	SelectObject(hdc, curPen);
	DeleteObject(selectSquarePen);
}

bool EllipseObj::isClickToSelect(int xClient, int yClient)
{
	if (isBeingSelected)
	{
		return EllipseObj::isClickToSelectNormal(xClient, yClient);
	}
	return EllipseObj::isClickToSelectEnhanced(xClient, yClient);
}

bool EllipseObj::isClickToSelectNormal(int xClient, int yClient)
{
	normalizeRect(&rect);
	RECT outsideRect;
	outsideRect.left = this->rect.left - EPSILON_CLICK_POINT_ELLIPSE;
	outsideRect.top = this->rect.top - EPSILON_CLICK_POINT_ELLIPSE;
	outsideRect.right = this->rect.right + EPSILON_CLICK_POINT_ELLIPSE;
	outsideRect.bottom = this->rect.bottom + EPSILON_CLICK_POINT_ELLIPSE;
	auto ellipseRgn = CreateEllipticRgnIndirect(&outsideRect);
	if (PtInRegion(ellipseRgn, xClient, yClient))
	{
		DeleteObject(ellipseRgn);
		return true;

	}
	DeleteObject(ellipseRgn);
	return false;
}

bool EllipseObj::isClickToSelectEnhanced(int xClient, int yClient)
{
	normalizeRect(&rect);

	RECT outsideRect;
	RECT insideRect;
	//Initialize outside Rect
	outsideRect.left = this->rect.left - EPSILON_CLICK_POINT_ELLIPSE;
	outsideRect.top = this->rect.top - EPSILON_CLICK_POINT_ELLIPSE;
	outsideRect.right = this->rect.right + EPSILON_CLICK_POINT_ELLIPSE;
	outsideRect.bottom = this->rect.bottom + EPSILON_CLICK_POINT_ELLIPSE;
	//Initialize inside Rect
	insideRect.left = this->rect.left + EPSILON_CLICK_POINT_ELLIPSE;
	insideRect.top = this->rect.top + EPSILON_CLICK_POINT_ELLIPSE;
	insideRect.right = this->rect.right - EPSILON_CLICK_POINT_ELLIPSE;
	insideRect.bottom = this->rect.bottom - EPSILON_CLICK_POINT_ELLIPSE;
	auto outEllipse = CreateEllipticRgnIndirect(&outsideRect);
	auto inEllipse = CreateEllipticRgnIndirect(&insideRect);
	if (PtInRegion(outEllipse, xClient, yClient) && !PtInRegion(inEllipse, xClient, yClient))
	{
		DeleteObject(outEllipse);
		DeleteObject(inEllipse);
		return true;
	}
	DeleteObject(outEllipse);
	DeleteObject(inEllipse);
	return false;
}


void EllipseObj::move(int oldXClient, int oldYClient, int xClient, int yClient)
{
	int deltaX = xClient - oldXClient;
	int deltaY = yClient - oldYClient;
	this->rect.left += deltaX;
	this->rect.top += deltaY;
	this->rect.right += deltaX;
	this->rect.bottom += deltaY;
}

void EllipseObj::drawSelectedStyle(HDC hdc)
{
	DrawFocusRect(hdc, &rect);
	RECT lftpRect;
	RECT rgtpRect;
	RECT lfbtRect;
	RECT rgbtRect;

	//Initialize left top corner rect
	POINT pt;
	pt.x = rect.left;
	pt.y = rect.top;
	fillRectAroundPoint(&lftpRect, pt, FOCUS_SQUARE_SIZE);
	//Initialize right top corner rect
	pt.x = rect.right;
	pt.y = rect.top;
	fillRectAroundPoint(&rgtpRect, pt, FOCUS_SQUARE_SIZE);
	//Initilize left bottom corner rect
	pt.x = rect.left;
	pt.y = rect.bottom;
	fillRectAroundPoint(&lfbtRect, pt, FOCUS_SQUARE_SIZE);
	//Initialize right bottom corner rect
	pt.x = rect.right;
	pt.y = rect.bottom;
	fillRectAroundPoint(&rgbtRect, pt, FOCUS_SQUARE_SIZE);
	//Draw the squares
	Rectangle(hdc, lftpRect.left, lftpRect.top, lftpRect.right, lftpRect.bottom);
	Rectangle(hdc, lfbtRect.left, lfbtRect.top, lfbtRect.right, lfbtRect.bottom);
	Rectangle(hdc, rgtpRect.left, rgtpRect.top, rgtpRect.right, rgtpRect.bottom);
	Rectangle(hdc, rgbtRect.left, rgbtRect.top, rgbtRect.right, rgbtRect.bottom);
}

void EllipseObj::setBeingSelected(bool flag)
{
	isBeingSelected = flag;
}

void EllipseObj::setColor(COLORREF color)
{
	if (this->color == color && this->_pen != NULL)
		return;
	this->color = color;
	DeleteObject(this->_pen);
	this->_pen = CreatePen(PS_SOLID, PEN_WIDTH, this->color);
}

bool EllipseObj::isClickToEdit(int xClient, int yClient)
{
	RECT smallRect;
	POINT ptClick;
	ptClick.x = xClient;
	ptClick.y = yClient;
	setBeingSelected(true);
	//Initialize left top corner rect
	POINT pt;
	pt.x = rect.left;
	pt.y = rect.top;
	fillRectAroundPoint(&smallRect, pt, FOCUS_SQUARE_SIZE);
	if (PtInRect(&smallRect, ptClick))
	{
		this->_recentCorner = CORNER_LT;
		return true;
	}
	//Initialize right top corner rect
	pt.x = rect.right;
	pt.y = rect.top;
	fillRectAroundPoint(&smallRect, pt, FOCUS_SQUARE_SIZE);
	if (PtInRect(&smallRect, ptClick))
	{
		this->_recentCorner = CORNER_RT;
		return true;
	}
	//Initilize left bottom corner rect
	pt.x = rect.left;
	pt.y = rect.bottom;
	fillRectAroundPoint(&smallRect, pt, FOCUS_SQUARE_SIZE);
	if (PtInRect(&smallRect, ptClick))
	{
		this->_recentCorner = CORNER_LB;
		return true;
	}
	//Initialize right bottom corner rect
	pt.x = rect.right;
	pt.y = rect.bottom;
	fillRectAroundPoint(&smallRect, pt, FOCUS_SQUARE_SIZE);
	if (PtInRect(&smallRect, ptClick))
	{
		this->_recentCorner = CORNER_RB;
		return true;
	}
	return false;
}

void EllipseObj::edit(HDC hdc,int oldXClient, int oldYClient, int xClient, int yClient)
{
	int deltaX = xClient - oldXClient;
	int deltaY = yClient - oldYClient;
	if (this->_recentCorner == CORNER_LT)
	{
		rect.left += deltaX;
		rect.top += deltaY;
		if (rect.left > rect.right)
		{
			if (rect.top < rect.bottom)
			{
				this->_recentCorner = CORNER_RT;
			}
			else
			{
				this->_recentCorner = CORNER_RB;
			}
		}
		else
		{
			if (rect.top > rect.bottom)
				this->_recentCorner = CORNER_LB;
		}
	}
	else if (this->_recentCorner == CORNER_RB)
	{
		rect.right += deltaX;
		rect.bottom += deltaY;
		if (rect.left > rect.right)
		{
			if (rect.top < rect.bottom)
			{
				this->_recentCorner = CORNER_LB;
			}
			else
			{
				this->_recentCorner = CORNER_LT;
			}
		}
		else
		{
			if (rect.top > rect.bottom)
				this->_recentCorner = CORNER_RT;
		}
	}
	else if (this->_recentCorner == CORNER_LB)
	{
		rect.left += deltaX;
		rect.bottom += deltaY;
		if (rect.left > rect.right)
		{
			if (rect.top < rect.bottom)
			{
				this->_recentCorner = CORNER_RB;
			}
			else
			{
				this->_recentCorner = CORNER_RT;
			}
		}
		else
		{
			if (rect.top > rect.bottom)
				this->_recentCorner = CORNER_LT;
		}
	}
	else if (this->_recentCorner == CORNER_RT)
	{
		rect.right += deltaX;
		rect.top += deltaY;
		if (rect.left > rect.right)
		{
			if (rect.top < rect.bottom)
			{
				this->_recentCorner = CORNER_LT;
			}
			else
			{
				this->_recentCorner = CORNER_LB;
			}
		}
		else
		{
			if (rect.top > rect.bottom)
				this->_recentCorner = CORNER_RB;
		}
	}
	normalizeRect(&this->rect);
}

void EllipseObj::setData(int x1, int y1, int x2, int y2)
{
	rect.left = x1;
	rect.top = y1;
	rect.right = x2;
	rect.bottom = y2;
}

void EllipseObj::setAnchor(int x, int y)
{
	auto width = rect.right - rect.left;
	auto height = rect.bottom - rect.top;
	rect.left = x;
	rect.top = y;
	rect.right = rect.left + width;
	rect.bottom = rect.top + height;
}

Shape * EllipseObj::cloneRetPtr()
{
	EllipseObj* copObj = new EllipseObj();
	copObj->setColor(this->color);
	auto width = this->rect.right - this->rect.left;
	auto height = this->rect.bottom - this->rect.top;
	std::random_device rd;
	std::mt19937 mt(rd());
	std::uniform_real_distribution<double> dis(-3, 3);
	auto delta = dis(mt)*PADDING_CC;
	auto delta1 = dis(mt)*PADDING_CC;
	((EllipseObj*)copObj)->rect.left = rect.left + delta;
	((EllipseObj*)copObj)->rect.top = rect.top + delta1;
	((EllipseObj*)copObj)->rect.right = ((EllipseObj*)copObj)->rect.left + width;
	((EllipseObj*)copObj)->rect.bottom = ((EllipseObj*)copObj)->rect.top + height;
	return copObj;
}

UINT EllipseObj::size()
{
	return sizeof(EllipseObj);
}

bool EllipseObj::saveToFile(HANDLE hFile)
{
	normalizeRect(&this->rect);
	if (!WriteFile(hFile, &this->type, sizeof(UINT), NULL, NULL))
		return false;
	if (!WriteFile(hFile, &this->color, sizeof(COLORREF), NULL, NULL))
		return false;
	if (!WriteFile(hFile, &this->rect, sizeof(RECT), NULL, NULL))
		return false;
	return true;
}

bool EllipseObj::loadFromFile(HANDLE hFile)
{
	COLORREF color;
	if (!ReadFile(hFile, &color, sizeof(COLORREF), NULL, NULL))
		return false;
	EllipseObj::setColor(color);
	if (!ReadFile(hFile, &this->rect, sizeof(RECT), NULL, NULL))
		return false;
	return true;
}


