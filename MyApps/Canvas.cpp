#include "stdafx.h"
#include "Canvas.h"
#include "AppsDef.h"
#include <commdlg.h>
#include "EllipseObject.h"
#include "LineObject.h"
#include "TextObject.h"
#include "RectangleObject.h"
void Canvas::draw(HDC hdc)
{
	
	for (int i = 0; i < this->_shapeArr.size(); ++i)
	{
		_shapeArr[i]->draw(hdc);
	}
}

void Canvas::setDrawColor(const COLORREF & color)
{
	this->_drawColor = color;
}

COLORREF Canvas::getDrawColor()
{
	return this->_drawColor;
}

void Canvas::setFont(const LOGFONT& font)
{
	_font = font;
}

LOGFONT Canvas::getFont()
{
	return _font;
}

void Canvas::setDrawMode(const UINT drawMode)
{
	_drawMode = drawMode;
}

UINT Canvas::getDrawMode()
{
	return _drawMode;
}
UINT Canvas::setSelectObject(int xClient, int yClient)
{
	Shape* tmpObj = nullptr;
	UINT conflObj = 0;
	for (int i = _shapeArr.size() - 1; i >= 0; --i)
	{
		Shape* shape = _shapeArr[i];
		//check if there is any shape is selected by the click
		if (shape->isClickToSelectNormal(xClient, yClient))
		{
			conflObj++;
			if (conflObj == 2)
				break;
			tmpObj = shape;
		}
	}
	if (conflObj == 1)
	{
		if (_selectedObj == tmpObj)
			return SAME_OBJECT;
		deselSelectObject();
		tmpObj->setBeingSelected(true);
		_selectedObj = tmpObj;
		_timeCopied = 0;
		return DIFF_OBJECT;
	}
	else if (conflObj == 0)
	{
		deselSelectObject();
		return NONE;
	}
	for (int i = _shapeArr.size() - 1; i >= 0; --i)
	{
		Shape* shape = _shapeArr[i];
		//check if there is any shape is selected by the click
		if (shape->isClickToSelectEnhanced(xClient, yClient))
		{
			if (_selectedObj == shape)
				return SAME_OBJECT;
			deselSelectObject();
			shape->setBeingSelected(true);
			_selectedObj = shape;
			_timeCopied = 0;
			return DIFF_OBJECT;
		}
	}
	if (_selectedObj != nullptr)
		return SAME_OBJECT;
	return NONE;
}

BOOL Canvas::deselSelectObject()
{
	OutputDebugStringA("Deselect Selected Object\n");
	if (_selectedObj != nullptr)
	{
		OutputDebugStringA("Set being selected to false\n");
		_selectedObj->setBeingSelected(false);
		_selectedObj = nullptr;
		return TRUE;
	}
	return FALSE;
}

bool Canvas::addNewObject(Shape * shape)
{
	try {
		_shapeArr.push_back(shape);
	}
	catch (...)
	{
		return false;
	}
	return true;
}

void Canvas::moveSelectObject(int xClient, int yClient)
{
	if (_selectedObj != nullptr)
	{
		_selectedObj->move(this->_xRecentClick, this->_yRecentClick, xClient, yClient);
	}
}

Shape * Canvas::getLatestObject()
{
	if (this->_shapeArr.size() == 0)
		return nullptr;
	return this->_shapeArr.back();
}

void Canvas::editSelectedObject(int xClient, int yClient)
{
	if (_selectedObj == nullptr)
		return;
	HDC hdc = GetDC(this->_hWnd);
	_selectedObj->edit(hdc,_xRecentClick, _yRecentClick, xClient, yClient);
	ReleaseDC(this->_hWnd,hdc);
}

HANDLE Canvas::getFileHandle()
{
	return this->fileHandle;
}
void Canvas::setFileHandle(HANDLE file)
{
	this->fileHandle = file;
}
bool Canvas::saveToFile()
{
	if (this->fileHandle == INVALID_HANDLE_VALUE)
		return false;
	if (!WriteFile(this->fileHandle, &this->_drawColor, sizeof(COLORREF), NULL, NULL))
	{
		return false;
	}
	if (!WriteFile(this->fileHandle, &this->_font, sizeof(LOGFONT), NULL, NULL))
	{
		return false;
	}
	if (!WriteFile(this->fileHandle, &this->_drawMode, sizeof(UINT), NULL, NULL))
		return false;
	if (!WriteFile(this->fileHandle, &this->_editMode, sizeof(UINT), NULL, NULL))
		return false;
	auto size = _shapeArr.size();
	if (!WriteFile(this->fileHandle, &size, sizeof(UINT), NULL, NULL))
		return false;
	for (auto i = 0; i < _shapeArr.size(); ++i)
	{
		if (!_shapeArr[i]->saveToFile(this->fileHandle))
		{
			return false;
		}
	}
}

UINT Canvas::getEditMode()
{
	return this->_editMode;
}

void Canvas::setEditMode(UINT editMode)
{
	this->_editMode = editMode;
}

void Canvas::remove(Shape * shape)
{
	if (!_shapeArr.empty())
	{
		std::remove(_shapeArr.begin(), _shapeArr.end(), shape);
		_shapeArr.pop_back();
		delete shape;
		_selectedObj = nullptr;
	}
		
}

bool Canvas::loadDataFromFile()
{
	this->_isSaved = true;
	if (this->fileHandle == INVALID_HANDLE_VALUE)
		return false;
	if (!ReadFile(this->fileHandle, &this->_drawColor, sizeof(COLORREF), NULL, NULL))
	{
		return false;
	}
	if (!ReadFile(this->fileHandle, &this->_font, sizeof(LOGFONT), NULL, NULL))
	{
		return false;
	}
	if (!ReadFile(this->fileHandle, &this->_drawMode, sizeof(UINT), NULL, NULL))
		return false;
	if (!ReadFile(this->fileHandle, &this->_editMode, sizeof(UINT), NULL, NULL))
		return false;
	UINT size{};
	if (!ReadFile(this->fileHandle, &size, sizeof(UINT), NULL, NULL))
		return false;
	for (int i = 0; i < size; ++i)
	{
		UINT type;
		if (!ReadFile(this->fileHandle, &type, sizeof(UINT), NULL, NULL))
		{
			return false;
		}
		if (type == LINE)
		{
			Shape* shape = new LineObj();
			if (!shape->loadFromFile(this->fileHandle))
				return false;
			_shapeArr.push_back(shape);
		}
		else if (type == RECTANGLE)
		{
			Shape* shape = new RectObj();
			if (!shape->loadFromFile(this->fileHandle))
				return false;
			_shapeArr.push_back(shape);
		}
		else if (type == ELLIPSE)
		{
			Shape* shape = new EllipseObj();
			if (!shape->loadFromFile(this->fileHandle))
				return false;
			_shapeArr.push_back(shape);
		}
		else if (type == TEXT)
		{
			Shape* shape = new TextObj();
			if (!shape->loadFromFile(this->fileHandle))
				return false;
			_shapeArr.push_back(shape);
		}
	}
}

void Canvas::freeResource()
{
	for (int i = 0; i < _shapeArr.size(); ++i)
	{
		delete _shapeArr[i];
		_shapeArr[i] = nullptr;
	}
	if (fileHandle != INVALID_HANDLE_VALUE)
		CloseHandle(fileHandle);
	fileHandle = INVALID_HANDLE_VALUE;
}

void Canvas::initResource()
{
	this->fileHandle = INVALID_HANDLE_VALUE;
	this->_drawColor = RGB(0, 0, 0);
	this->_drawMode = DRAWMODE_LINE;
	this->_editMode = EDITMODE_DRAW;
	GetObject(GetStockObject(SYSTEM_FONT),sizeof(LOGFONT),&this->_font);
	this->_hWnd = NULL;
	this->_isClickToDrawObject = false;
	this->_isClickToEditSelectObject = false;
	this->_isClickToMoveObject = false;
	this->_isMaximized = false;
	this->_isObjectCreated = false;
	this->_selectedObj = nullptr;
	this->_xRecentClick = 0;
	this->_yRecentClick = 0;
	this->_isSaved = true;
}

Shape * Canvas::getSelectedObject()
{
	return this->_selectedObj;
}
