#pragma once
#include "Shape.h"
#include "AppsDef.h"
class TextObj : public Shape
{
public:
	TextObj(COLORREF color);
	TextObj();
	~TextObj();
	virtual void draw(HDC hdc); //Draw an object using the parent window HDC
	virtual bool isClickToSelect(int xClient, int yClient); //check the point that user click selecting any object
	bool isClickToSelectNormal(int xClient, int yClient);
	bool isClickToSelectEnhanced(int xClient, int yClient);
	virtual void move(int oldXClient, int oldYClient, int xClient, int yClient);
	virtual void setBeingSelected(bool flag);
	virtual void setColor(COLORREF color);
	virtual void edit(HDC hdc,int oldXClient, int oldYClient, int xClient, int yClient);
	virtual bool isClickToEdit(int xClient, int yClient);
	virtual void drawSelectedStyle(HDC hdc);
	virtual void setData(int x1, int y1, int x2, int y2);
	void virtual setText(const std::wstring& newContent);
	void virtual setText(std::wstring&& newContent);
	void virtual setFont(const LOGFONT& font);
	void setAnchor(int x, int y);
	LOGFONT getFont();
	const std::wstring& getText();
	bool saveToFile(HANDLE hFile);
	bool loadFromFile(HANDLE hFile);
	Shape* cloneRetPtr();
	UINT size();
private:
	//Class Member
	std::wstring _content;
	LOGFONT _font;
	HFONT _hFont;
	RECT rect;
	UINT _recentCorner;
};